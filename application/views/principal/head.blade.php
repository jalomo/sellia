<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
<meta http-equiv="X-UA-Compatible" content="ie=edge"/>
<title>SELLIA</title>
<!-- CSS files -->

<link href="<?php echo base_url();?>template/tema/css/tabler.min.css" rel="stylesheet"/>
<link href="<?php echo base_url();?>template/tema/dist/css/tabler-flags.min.css" rel="stylesheet"/>
<link href="<?php echo base_url();?>template/tema/css/tabler-payments.min.css" rel="stylesheet"/>
<link href="<?php echo base_url();?>template/tema/css/tabler-vendors.min.css" rel="stylesheet"/>
<link href="<?php echo base_url();?>template/tema/css/demo.min.css" rel="stylesheet"/>