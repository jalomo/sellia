<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-beta6
* @link https://tabler.io
* Copyright 2018-2022 The Tabler Authors
* Copyright 2018-2022 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">
  <head>
  <link rel="icon" href="<?php echo base_url();?>icon.png">
  @include('principal/head')

  <style>
      body{
        background-image: url("https://iplaneacion.com.mx/dms/queretaro_dms/luna/images/background-main.png");
      }
    </style>
 
  </head>
  <body >
    <div class="page">
		@include('principal/header_menu')

		@yield('header1')
    

        <div class="page-body">
         @yield('contenido')
        </div>
        <footer class="footer footer-transparent d-print-none">
        @include('principal/footer')
        </footer>
      </div>
    </div>

    @yield('modales')

    @include('principal/modale_ejemplo')
    
    <!-- Libs JS -->
    <script src="<?php echo base_url();?>template/tema/libs/apexcharts/dist/apexcharts.min.js"></script>
    <script src="<?php echo base_url();?>template/tema/libs/jsvectormap/dist/js/jsvectormap.min.js"></script>
    <script src="<?php echo base_url();?>template/tema/libs/jsvectormap/dist/maps/world.js"></script>
    <!-- Tabler Core -->
    <script src="<?php echo base_url();?>template/tema/js/tabler.min.js"></script>
    <script src="<?php echo base_url();?>template/tema/js/demo.min.js"></script>
	@yield('javascript')
    
  </body>
</html>