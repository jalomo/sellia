<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Login_user extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        //$this->load->model('Minicio', '', TRUE);
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url','general'));

        date_default_timezone_set('America/Mexico_City');
        

    }

    public function index()
    {
        
        $datos['error'] = '';

        if($this->input->post()){
            //This method will have the credentials validation
            //$this->load->library('form_validation');

            $this->form_validation->set_rules('username', 'Username', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');

            if($this->form_validation->run() == FALSE)
            {
                //Field validation failed.  User redirected to login page
                //$this->load->view('login_view');
                //$aside = $this->load->view('companies/left_menu', '', TRUE);
                $datos['error'] = 'usuario o contraseña incorrecto1';
                $this->blade->render('index_new',$datos);
            }
            else
            {

                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $result = $this->Mgeneral->login($username, $password);
                if($result)
                {
                    $sess_array = array();
                    foreach($result as $row)
                    {
                    $sess_array = array(
                        'id' => $row->id,
                        'username' => $row->username
                    );
                    $this->session->set_userdata('logged_in', $sess_array);
                    }
                    //Go to private area
                    redirect('sucursales/lista', 'refresh');
                }
                else
                {
                    $datos['error'] = 'usuario o contraseña incorrecto2';
                    $this->blade->render('index_new',$datos);
                }


                
            }

        }else{
            $this->blade->render('index_new',$datos); 
        }

    }


    function check_database($password)
    {
        //Field validation succeeded.  Validate against database
        $username = $this->input->post('username');

        //query the database
        $result = $this->Mgeneral->login($username, $password);

        if($result)
        {
            $sess_array = array();
            foreach($result as $row)
            {
            $sess_array = array(
                'id' => $row->id,
                'username' => $row->username
            );
            $this->session->set_userdata('logged_in', $sess_array);
            }
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('check_database', 'Usuario o contraseña inválidos');
            return false;
        }
    }
    function logout()
    {
    $this->session->set_userdata('logged_in', null);
    redirect('/', 'refresh');
    }

    public function index2(){

        $datos['index'] = '';
        $datos['cuentas'] = '';
        $datos['error'] = '';

        

        if($this->input->post()){
            if($this->input->post('email') && $this->input->post('password') ){

                $login1 = $this->Mgeneral->login_usuario($this->input->post('email'), $this->input->post('password') );
                if(is_object($login1)){
                    $usuario_cuenta = $this->Mgeneral->get_row('id',$login1->id,'usuarios');
                    if(is_object($usuario_cuenta)){
                        if($usuario_cuenta->status == 1){
                            redirect('inicio/');

                        }else{
                            $datos['error'] = 'Cuenta desactivada.';
                            $this->blade->render('index_new',$datos);
                        }

                    }else{
                        $datos['error'] = 'Cuenta no existe.';
                        $this->blade->render('index_new',$datos);
                    }
                    

                }else{
                    $datos['error'] = 'usuario y contraseña son incorrectos.';
                    $this->blade->render('index_new',$datos); 

                }


            }else{
                $datos['error'] = 'usuario y contraseña son obligatorios.';
                $this->blade->render('index_new',$datos); 
            }

        }else{
            $this->blade->render('index_new',$datos); 
        }
         
        
    }
}