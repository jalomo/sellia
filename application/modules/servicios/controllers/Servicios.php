<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Servicios extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        //$this->load->model('Minicio', '', TRUE);
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->model('Madmin', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url','general','companies'));

        date_default_timezone_set('America/Mexico_City');

       
        if($this->session->userdata('logged_in')){}else{redirect('login_user/');}
        

    }

    public function salir()
    {
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        redirect('login_user');
    }

    public function lista(){
        $datos['servicios'] = $this->Madmin->get_tabla_desc_asc('servicios','id','DESC');
        
        $datos['index'] = '';
        $datos['cuentas'] = '';
        $datos['error'] = '';

        $this->blade->render('servicios/lista',$datos);  
        
    }

    public function empList(){
     
        // POST data
        $postData = $this->input->post();
   
        // Get data
        $data = $this->Mgeneral->getEmployees($postData);
   
        echo json_encode($data);
     }
}