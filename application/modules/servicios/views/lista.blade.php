@layout('layout_main')
@section('contenido')

<div class="container-xl">
    <div class="row row-cards">
        <div class="col-12">
            <div class="card">
                <div class="table-responsive">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-vcenter card-table" >

                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Fecha creacion</th>
                      <th>Sucursal</th>
                      <th>Folio pedido</th>
                      <th>Descripcion</th>
                      <th>Nombre cliente</th>
                      <th>Coordenadas cliente</th>
                      <!--th>Tipo pedido</th-->
                      <th>Estatus pedido</th>
                      <th>Operador</th>
                      <th>Precio</th>
                     
                    </tr>
                  </thead>
                  <tbody>
                        <?php foreach($servicios as $row):?>
                        <tr>
                          <td > <?php echo $row->id;?></td>
                          <td > <?php echo $row->fecha_creacion;?></td>
                          <td > <?php echo nombre_sucursal($row->id_sucursal);?></td>
                          <td > <?php echo $row->folio_pedido;?></td>
                          <td > <?php echo $row->descripcion;?></td>
                          <td > <?php echo $row->nombre_cliente;?></td>
                          <td > <?php echo $row->latitud;?>,<?php echo $row->longitud;?></td>
                          <td > <?php echo $row->tipo_pedido;?></td>
                          <!--td > 
                            <?php  if($row->estatus_pedido == 1){echo "MOTO";}?>
                          </td-->
                          <td > <?php echo nombr_operador($row->id_operador);?></td>
                          <td > <?php echo $row->precio;?></td>
                        
                          
                         
                          
                        </tr>
                        <?php endforeach;?>
                        

                      
                       
                        
                      </tbody>

                  </table>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection



@section('header1')


@endsection

@section('modales')

    

@endsection


@section('javascript')
<!-- Datatable CSS -->
<link href='//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

<!-- jQuery Library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Datatable JS -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
/*

$(document).ready(function(){
        $('#empTable').DataTable({
          'processing': true,
          'serverSide': true,
          'serverMethod': 'post',
          'ajax': {
             'url':'<?=base_url()?>index.php/servicios/empList'
          },
          'columns': [
           
             { data: 'id'},
             { data: 'usuario' },
             { data: 'telefono' },
             { data: 'fecha_creacion' },
             { data: 'status' },
             { data: 'nombre_operador' },
             { data: 'tarifa' },
             { data: 'inicio',render: function(data, type) {
                    if (type === 'display') {

                        let link = data;// "http://datatables.net";
 
                        return '<a target="_blank" href="http://www.google.com/maps/place/href="' + data + '" class="btn btn-primary">inicio</a>';
                    }
                     
                    return data;
                }
                
                 },
             { data: 'fin',render: function(data, type) {
                    if (type === 'display') {

                        let link = data;// "http://datatables.net";
 
                        return '<a target="_blank" href="http://www.google.com/maps/place/href="' + data + '" class="btn btn-success">fin</a>';
                    }
                     
                    return data;
                }
                
                 },
             
          ]
        });
     });
     */

</script>
@endsection


