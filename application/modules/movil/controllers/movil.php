<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class movil extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_movil', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'url'));

    }

   public function login(){
     $username = $this->input->post('username');
     $password = $this->input->post('password');
     $res = $this->m_movil->get_login($username,$password);
     echo $res;
   }

   public function buscarzona($latitud, $longitud, $zona){
     $this->load->library('pointLocation');
     $pointLocation = new pointLocation();
     $status = '';
     $points = array("".$latitud." ".$longitud."");
     $polygon =$zona;
     foreach($points as $key => $point) {
      $status = $pointLocation->pointInPolygon($point, $polygon);
    }
    return $status;
    }

    public function get_zona($id_user){
      $latitud = $this->input->post('latitud');
      $longitud =  $this->input->post('longitud');
      $res = $this->m_movil->get_result('poligonoIdUser',$id_user,'poligonos');
      $response = array();
      foreach($res as $row):
        $res=array();
        $res=$row->poligonoLatitud." ".$row->poligonoLongitud;
        array_push($response, $res);
      endforeach;

       $status =  $this->buscarzona($latitud, $longitud, $response);
       $data['registroStatus'] = $status;
       $data['registroIdUser'] = $id_user;
       $data['registroFecha'] = date('Y-m-d H:i:s');
       $data['registroLatitud'] = $latitud;
       $data['registroLongitud'] = $longitud;
       $this->m_movil->save_register('registros', $data);
       echo $status;

    }


}
