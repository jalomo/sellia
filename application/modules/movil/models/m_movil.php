<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_movil extends CI_Model {


	    public function __construct(){
	        parent::__construct();
	    }

	   public function get_login($username,$password){
       $this->db->where('userUsername',$username);
       $this->db->where('userPassword',$password);
       $query =$this->db->get('users');
       if($query->num_rows()>0){
         $res = $query->row();
         return $res->userId;
       }else{
         return 0;
       }
     }

		 public function save_register($table, $data)
       {
           $this->db->insert($table, $data);
           return $this->db->insert_id();
       }

       public function update($campo,$value,$tabla,$data){
     		return $this->db->where($campo,$value)->update($tabla,$data);
     	}
     	public function delete($campo,$value,$tabla){
     		return $this->db->where($campo,$value)->delete($tabla);
     	}
     	public function get_result($campo,$value,$tabla){
     		return $this->db->where($campo,$value)->get($tabla)->result();
     	}
     	public function get_row($campo,$value,$tabla){
     		return $this->db->where($campo,$value)->get($tabla)->row();
     	}
     	public function get_table($tabla){
     		return $this->db->get($tabla)->result();
     	}
}
