@layout('layout_main')
@section('contenido')

<div class="container-xl">
    <div class="row row-cards">
        <div class="col-12">
            <div class="card">
                <div class="table-responsive">


                <table cellpadding="0" cellspacing="0" border="0" class="table table-vcenter card-table" >
                  <thead>
                    <tr>
                      <th>Imagen</th>
                      <th>Nick</th>
                      <th>Placas</th>
                      <th>Modelo</th>
                      <th>Marca</th>
                      <th>Color</th>
                      <th>Email</th>
                      <th>Dueño</th>
                      <th>Opciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach( $rows as $row):?>
                    <tr class="odd gradeX">
                      <td>
                        <img src="<?php echo base_url().'/'.$row->foto;?>" width="100px"/>
                        </td>
                        <td><?php echo $row->nick;?></td>
                      <td><?php echo $row->placas;?></td>
                      <td><?php echo $row->modelo;?></td>
                      <td><?php echo $row->marca;?></td>
                      <td class="center"> <?php echo $row->color;?></td>
                      <td class="center"> <?php echo $row->email;?></td>
                      <td class="center"> <?php echo nombre_socio($row->id_dueno);?></td>
                      <td class="center">
                      <a href="<?php echo base_url()?>index.php/admin/editar_vehiculo/<?php echo $row->id;?>" class="btn btn-info">Editar</a>
                      
                        <a class="eliminar"  href="#" flag="<?php echo $row->id;?>" id="delete<?php echo $row->id;?>" >
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>

                      </td>
                    </tr>
                  <?php endforeach;?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>Placas</th>
                      <th>Modelo</th>
                      <th>Marca</th>
                      <th>Color</th>
                      <th>Email</th>
                      <th>Dueño</th>
                      <th>Opciones</th>
                    </tr>
                  </tfoot>
                </table>


                




                </div>
            </div>
        </div>
    </div>
</div>

@endsection



@section('header1')

<div class="container-xl">
          <!-- Page title -->
          <div class="page-header d-print-none">
            <div class="row align-items-center">
              <div class="col">
                <!-- Page pre-title -->
                <div class="page-pretitle">
                  Lista de Vehículos
                </div>
               

                

              </div>
              <!-- Page title actions -->
              <div class="col-auto ms-auto d-print-none">
                <div class="btn-list">
                  <!--pan class="d-none d-sm-inline">
                    <a href="#" class="btn btn-white">
                      New view
                    </a>
                  </span-->
                  <a href="#" class="btn btn-primary d-none d-sm-inline-block" data-bs-toggle="modal" data-bs-target="#modal-sucursal">
                    <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
                    Nuevo Vehículo
                  </a>
                  <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-sucursal" aria-label="Create new report">
                    <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

@section('modales')



    <!-- equipo -->

    <div class="modal modal-blur fade" id="modal-sucursal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Nuevo Vehículo</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>

          <form id="formulario1">
            <div class="modal-body">
              <div class="mb-3">
                <label class="form-label">Nombre</label>
                <input type="text" class="form-control"  placeholder="" name="nombre">
              </div>  
              <div class="mb-3">
                <label class="form-label">Teléfono</label>
                <input type="text" class="form-control"  placeholder="" name="nombre">
              </div> 
              <div class="mb-3">
                <label class="form-label">Usuario</label>
                <input type="text" class="form-control"  placeholder="" name="nombre">
              </div>   
              <div class="mb-3">
                <label class="form-label">Contraseña</label>
                <input type="text" class="form-control"  placeholder="" name="nombre">
              </div>  
              <div class="mb-3">
                <label class="form-label">Foto</label>
                <input type="text" class="form-control"  placeholder="" name="nombre">
              </div>  
              <div class="mb-3">
                <label class="form-label">Sucursal</label>
                <select class="form-control"  >
                    <option></option>
                </select>
              </div>            
            </div>
            
            <div class="modal-footer">
              
              <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                Cancel
              </a>
              <a href="#" class="btn btn-primary ms-auto" data-bs-dismiss="modal" id="alta_crear1">
                <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
                Crear nueva sucursal
              </a>
            </div>

          </form>


        </div>
      </div>
    </div>


    

@endsection


@section('javascript')
<script src="<?php echo base_url();?>statics/js/libraries/jquery.js"></script>
<script src="<?php echo base_url();?>statics/js/bootbox.min.js"></script>
<script src="<?php echo base_url();?>statics/js/general.js"></script>
<script>


$('#alta_crear1').click(function(event){
  event.preventDefault();
  $("#btn_guardar").hide();
  $("#btn_cargando").show();
  var url_sis ="<?php echo base_url()?>index.php/sucursales/guarda";

  // Get form
        var form = $('#formulario1')[0];

		// Create an FormData object
        var data = new FormData(form);

  $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: url_sis,
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (result) {
                console.log(result);
                json_response = JSON.parse(result);
                obj_output = json_response.output;
                obj_status = obj_output.status;
                if(obj_status == false){
                  aux = "";
                  $.each( obj_output.errors, function( key, value ) {
                    aux +=value+"<br/>";
                  });
                  exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
                  $("#btn_guardar").show();
                  $("#btn_cargando").hide();
                }
                if(obj_status == true){
                  exito_redirect("GUARDADO CON EXITO","success","<?php echo base_url()?>index.php/sucursales/lista");
                }

            },
            error: function (e) {
                $("#btn_guardar").show();
                $("#btn_cargando").hide();
                //$("#result").text(e.responseText);
                //console.log("ERROR : ", e);
                //$("#btnSubmit").prop("disabled", false);
                exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
                

            }
        });
});

</script>
@endsection


