<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Autos extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        //$this->load->model('Minicio', '', TRUE);
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->model('Madmin', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url','general','companies'));

        date_default_timezone_set('America/Mexico_City');
        

    }

    public function lista(){
        $datos['rows'] = $this->Madmin->select_result('auto','eliminado', 0);
        
        $datos['index'] = '';
        $datos['cuentas'] = '';
        $datos['error'] = '';

        $this->blade->render('autos/lista',$datos);  
        
    }
}