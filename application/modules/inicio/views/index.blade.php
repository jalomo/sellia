@layout('layout_main')



@section('contenido')
<div class="container-xl">
           

            <div class="col-12">
                <div class="row row-cards">
                  <div class="col-sm-6 col-lg-3">
                    <div class="card card-sm">
                      <div class="card-body">
                        <div class="row align-items-center">
                          <div class="col-auto">
                            <span class="bg-blue text-white avatar"><!-- Download SVG icon from http://tabler-icons.io/i/currency-dollar -->
                              <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M16.7 8a3 3 0 0 0 -2.7 -2h-4a3 3 0 0 0 0 6h4a3 3 0 0 1 0 6h-4a3 3 0 0 1 -2.7 -2" /><path d="M12 3v3m0 12v3" /></svg>
                            </span>
                          </div>
                          <div class="col">
                            <div class="font-weight-medium">
                              132 Animales
                            </div>
                            <div class="text-muted">
                              <a href="#" data-bs-toggle="modal" data-bs-target="#modal-animal">Agregar animal</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-lg-3">
                    <div class="card card-sm">
                      <div class="card-body">
                        <div class="row align-items-center">
                          <div class="col-auto">
                            <span class="bg-green text-white avatar"><!-- Download SVG icon from http://tabler-icons.io/i/shopping-cart -->
                              <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><circle cx="6" cy="19" r="2" /><circle cx="17" cy="19" r="2" /><path d="M17 17h-11v-14h-2" /><path d="M6 5l14 1l-1 7h-13" /></svg>
                            </span>
                          </div>
                          <div class="col">
                            <div class="font-weight-medium">
                              78 Pastura
                            </div>
                            <div class="text-muted">
                            <a href="#" data-bs-toggle="modal" data-bs-target="#modal-pastura">Agregar pastura</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-lg-3">
                    <div class="card card-sm">
                      <div class="card-body">
                        <div class="row align-items-center">
                          <div class="col-auto">
                            <span class="bg-twitter text-white avatar"><!-- Download SVG icon from http://tabler-icons.io/i/brand-twitter -->
                              <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M22 4.01c-1 .49 -1.98 .689 -3 .99c-1.121 -1.265 -2.783 -1.335 -4.38 -.737s-2.643 2.06 -2.62 3.737v1c-3.245 .083 -6.135 -1.395 -8 -4c0 0 -4.182 7.433 4 11c-1.872 1.247 -3.739 2.088 -6 2c3.308 1.803 6.913 2.423 10.034 1.517c3.58 -1.04 6.522 -3.723 7.651 -7.742a13.84 13.84 0 0 0 .497 -3.753c-.002 -.249 1.51 -2.772 1.818 -4.013z" /></svg>
                            </span>
                          </div>
                          <div class="col">
                            <div class="font-weight-medium">
                              623 Actividades
                            </div>
                            <div class="text-muted">
                            <a href="#" data-bs-toggle="modal" data-bs-target="#modal-actividad">Agregar actividad</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-lg-3">
                    <div class="card card-sm">
                      <div class="card-body">
                        <div class="row align-items-center">
                          <div class="col-auto">
                            <span class="bg-facebook text-white avatar"><!-- Download SVG icon from http://tabler-icons.io/i/brand-facebook -->
                              <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M7 10v4h3v7h4v-7h3l1 -4h-4v-2a1 1 0 0 1 1 -1h3v-4h-3a5 5 0 0 0 -5 5v2h-3" /></svg>
                            </span>
                          </div>
                          <div class="col">
                            <div class="font-weight-medium">
                              132 Equipos
                            </div>
                            <div class="text-muted">
                            <a href="#" data-bs-toggle="modal" data-bs-target="#modal-equipo">Agregar equipo</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

</div>

@endsection

@section('modales')


<div class="modal modal-blur fade" id="modal-animal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Nuevo animal</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div class="row mb-3">

              
              <div class="col-lg-6">
                <label class="form-label">Nombre</label>
                <input type="text" class="form-control " name="example-text-input" placeholder="">
              </div>
              <div class="col-lg-6">
                <label class="form-label">Número registro</label>
                <input type="text" class="form-control " name="example-text-input" placeholder="">
              </div>

            </div>

            <div class="row mb-3">

              <div class="col-lg-3">
                <label class="form-label">Etiqueta</label>
                <input type="text" class="form-control" name="example-text-input" placeholder="">
              </div>
              <div class="col-lg-3">
              <label class="form-label">Ubicación</label>
               <select class="form-select">
                   <option value="1">Oreja izquierda</option>
                   <option value="2">Oreja derecha</option>
                   <option value="3">Ambas orejas</option>
               </select>
              </div>

              <div class="col-lg-3">
                <label class="form-label">Fierro</label>
                <input type="text" class="form-control" name="example-text-input" placeholder="">
              </div>
              <div class="col-lg-3">
              <label class="form-label">Ubicación</label>
               <select class="form-select">
                   <option value="1">Oreja izquierda</option>
                   <option value="2">Oreja derecha</option>
                   <option value="3">Ambas orejas</option>
               </select>
              </div>

            </div>

            <div class="row mb-3">

              
              <div class="col-lg-6">
                <div class="form-label">Imagen fierro</div>
                <input type="file" class="form-control" />
              </div>
              <div class="col-lg-6">
                <img src=""/>
              </div>

            </div>

            <div class="row mb-3">

              
              <div class="col-lg-3">
                <label class="form-label">Estado</label>
                    <select class="form-select">
                    <option value="1">Activo</option>
                    <option value="2">Vendido</option>
                    <option value="3">Muerto</option>
                    <option value="3">Referencia</option>
                </select>
                </div>
              <div class="col-lg-3">
                <label class="form-label">Tipo</label>
                <select class="form-select">
                    <option value="1">Vata</option>
                    <option value="2">Toro</option>
                    <option value="3">Ternero</option>
                </select>
              </div>

              <div class="col-lg-3">
                <label class="form-label">Sexo</label>
                <select class="form-select">
                    <option value="1">Toro</option>
                    <option value="2">Novilla</option>
                    <option value="3">Steer</option>
                </select>
              </div>

              <div class="col-lg-3">
                <label class="form-label">Categoria</label>
                <select class="form-select">
                    <option value="1">Agregar una categoria</option>
                    
                </select>
              </div>

            </div>

            <hr/>

            <label class="form-label">Becerro</label>


            <div class="row mb-3">

              <div class="col-lg-3">
                    <label class="form-label">Fecha de nacimiento</label>
                    <input type="text" class="form-control" />
                </div>
              <div class="col-lg-3">
                <label class="form-label">Peso al nacer</label>
                <input type="text" class="form-control" />
              </div>

              <div class="col-lg-3">
                <label class="form-label">Fecha destete</label>
                <input type="text" class="form-control" />
              </div>

              <div class="col-lg-3">
                <label class="form-label">Peso al destete</label>
                <input type="text" class="form-control" />
              </div>

            </div>

            <div class="row mb-3">
                <div class="col-lg-3">
                    <label class="form-label">Fecha de un año</label>
                    <input type="text" class="form-control" />
                </div>
                <div class="col-lg-3">
                    <label class="form-label">Peso de un año</label>
                    <input type="text" class="form-control" />
                </div>
            </div>


            <hr/>
            <label class="form-label">Descripción del animal</label>


            <div class="row mb-3">
                <div class="col-lg-3">
                    <label class="form-label">Raza</label>
                    <select class="form-select">
                     <option value="1">Agregar una categoria</option>
                    
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="form-label">Peso de un año</label>
                    <select class="form-select">
                     <option value="1">Color</option>
                    
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="form-label">Concepción</label>
                    <select class="form-select">
                     <option value="1">Natural</option>
                     <option value="1">Inseminación artificial</option>
                     <option value="1">Transferfencia de embrión</option>
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="form-label">Becerro injertado</label>
                    <select class="form-select">
                     <option value="1">SI</option>
                     <option value="1">NO</option>
                    </select>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-lg-3">
                    <label class="form-label">Patre</label>
                    <select class="form-select">
                     <option value="1">Agregar una categoria</option>
                    
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="form-label">Madre</label>
                    <select class="form-select">
                     <option value="1">Color</option>
                    
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="form-label">Parentesco de ADN</label>
                    <select class="form-select">
                     <option value="1">Madre verificada</option>
                     <option value="1">Padre verificado</option>
                     <option value="1">Sin excluciones</option>
                     <option value="1">Ambos expluidos</option>
                     <option value="1">Madre excluida</option>
                     <option value="1">Padre excluido </option>
                     <option value="1">Muestra fallida</option>
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="form-label">Comentarios</label>
                    <input type="text" class="form-control" />
                </div>
            </div>


            <hr/>
            <label class="form-label">Información de la compra</label>


            <div class="row mb-3">
                <div class="col-lg-3">
                    <label class="form-label">Comprado</label>
                    <select class="form-select">
                     <option value="1">SI</option>
                     <option value="1">NO</option>
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="form-label">Propietario de la compra</label>
                    <select class="form-select">
                     <option value="1"></option>
                    
                    </select>
                </div>
                
                <div class="col-lg-3">
                    <label class="form-label">Comentarios</label>
                    <input type="text" class="form-control" />
                </div>
            </div>



              

            


            

            


            
          </div>
        
          <div class="modal-footer">
            <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
              Cancel
            </a>
            <a href="#" class="btn btn-primary ms-auto" data-bs-dismiss="modal">
              <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
              <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
              Crear nuevo animal
            </a>
          </div>
        </div>
      </div>
    </div>







    <!-- pasturas -->

    <div class="modal modal-blur fade" id="modal-pastura" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Nueva pastura</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div class="mb-3">
              <label class="form-label">Nombre</label>
              <input type="text" class="form-control" name="example-text-input" placeholder="">
            </div>
            
            
            
            
          </div>
          
          <div class="modal-footer">
            <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
              Cancel
            </a>
            <a href="#" class="btn btn-primary ms-auto" data-bs-dismiss="modal">
              <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
              <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
              Crear nueva pastura
            </a>
          </div>
        </div>
      </div>
    </div>




    <!-- actividad -->

    <div class="modal modal-blur fade" id="modal-actividad" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Nueva actividad</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div class="mb-3">
              <label class="form-label">Nombre</label>
              <input type="text" class="form-control" name="example-text-input" placeholder="">
            </div>
            
            
            
            
          </div>
          
          <div class="modal-footer">
            <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
              Cancel
            </a>
            <a href="#" class="btn btn-primary ms-auto" data-bs-dismiss="modal">
              <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
              <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
              Crear nueva actividad
            </a>
          </div>
        </div>
      </div>
    </div>


    <!-- equipo -->

    <div class="modal modal-blur fade" id="modal-equipo" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Nuevo equipo</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div class="mb-3">
              <label class="form-label">Nombre</label>
              <input type="text" class="form-control" name="example-text-input" placeholder="">
            </div>
            
            
            
            
          </div>
          
          <div class="modal-footer">
            <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
              Cancel
            </a>
            <a href="#" class="btn btn-primary ms-auto" data-bs-dismiss="modal">
              <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
              <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
              Crear nuevo equipo
            </a>
          </div>
        </div>
      </div>
    </div>

@endsection