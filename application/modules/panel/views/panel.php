<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=drawing"></script>
<script>
var polygons = [];
var map; // Global declaration of the map
            var iw = new google.maps.InfoWindow(); // Global declaration of the infowindow
            var lat_longs = new Array();
            var markers = new Array();
            var drawingManager;
            function initialize() {
                 var myLatlng = new google.maps.LatLng(40.9403762, -74.1318096);
                var myOptions = {
                    zoom: 13,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP}
                map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
                drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.POLYGON,
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: [google.maps.drawing.OverlayType.POLYGON]
                },
                        polygonOptions: {
                            editable: true
                        }
            });
            drawingManager.setMap(map);

            google.maps.event.addListener(drawingManager, "overlaycomplete", function(event) {
                var newShape = event.overlay;
                newShape.type = event.type;
                 polygons.push(newShape);
            });

            google.maps.event.addListener(drawingManager, "overlaycomplete", function(event){
                overlayClickListener(event.overlay);
                $('#vertices').val(event.overlay.getPath().getArray());
            });
        }
function overlayClickListener(overlay) {
    google.maps.event.addListener(overlay, "mouseup", function(event){
        $('#vertices').val(overlay.getPath().getArray());
    });
}
google.maps.event.addDomListener(window, 'load', initialize);


$(function(){
    $('#save').click(function(){
        vertices = "";
        for (var i=0; i<polygons.length; i++) {
            //vertices += "polygon "+i+"<br>";
            for (var j=0; j<polygons[i].getPath().getLength(); j++) {
                vertices += polygons[i].getPath().getAt(j).toUrlValue(6)+",";
            }
        }

        value_json = $.ajax({
         type: "POST",
         url: '<?php echo base_url();?>index.php/panel/save_poligono',
         data:{poligono:vertices,idUser:$("#id_user").val()},
         async: true,
         dataType: "text",
          success: function(data){
            alert('poligono agregado');
            location.reload();
          }
         }).responseText;

    });
});

function getuser(id){
  $("#id_user").val(id);
}

function get_recort(id){
  value_json = $.ajax({
   type: "GET",
   url: '<?php echo base_url();?>index.php/panel/get_record/'+id,
   async: true,
   dataType: "html",
    success: function(data){
      $("#load").html(data);
    }
   }).responseText;
}
</script>
<div class="span9">
  <div class="content ">
    <div class="module">
      <div class="module-body">
      <form action="<?php echo base_url()?>index.php/panel/save_user" method="post">
      <div class="control-group">
        <label class="control-label" for="basicinput">Name</label>
        <div class="controls">
          <input type="text" id="noticiasTitulo" placeholder="Name..." class="span8" name="save[userName]">
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="basicinput">Username</label>
        <div class="controls">
          <input type="text" id="noticiasTitulo" placeholder="Username..." class="span8" name="save[userUsername]">
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="basicinput">Password</label>
        <div class="controls">
          <input type="password" id="noticiasTitulo" placeholder="Password..." class="span8" name="save[userPassword]">
        </div>
      </div>

      <button type="submit" class="btn btn-primary pull-right">Save</button>
      <br/>
      <hr/>
    </form>
    <input type="text" class="form-control input-sm m-b-xs" id="filter"
           placeholder="Search in table">
    <table class="table table-hover footable" data-page-size="8" data-filter="#filter">
      <thead>
        <tr>
          <th>Name</th>
          <th>Username</th>
          <th>Password</th>
          <th>Options</th>
          </tr>
        </thead>
          <tbody>
            <?php foreach($lista as $row):?>
             <tr id="">
               <td><?php echo $row->userName;?></td>
               <td><?php echo $row->userUsername;?></td>
               <td><?php echo $row->userPassword;?></td>
               <td>
                 <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#reg" onclick="get_recort(<?php echo $row->userId;?>)">records </button>
                 <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal" id="" onclick="getuser(<?php echo $row->userId;?>)">add poligono</button>
               </td>
             </tr>
           <?php endforeach;?>

          </tbody>

    </table>
     </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <form method="post" accept-charset="utf-8" id="map_form" action="<?php echo base_url()?>index.php/panel/save_poligono">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add poligon</h4>
      </div>
      <div class="modal-body">

        <div id="map-canvas" style="width:100%; height:300px !important;"></div>

        <input type="hidden" name="vertices" value="" id="vertices" />
        <input type="hidden" name="idUser" id="id_user"/>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save">Save </button>
      </div>
    </div>
  </div>
  </form>
</div>



<div class="modal fade" id="reg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Registros</h4>
      </div>
      <div class="modal-body">

        <div id="load"></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>
    </div>
  </div>

</div>
