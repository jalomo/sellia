<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class panel extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_panel', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'url'));
        if(!$this->session->userdata('id')){redirect('login');}
    }



	public function index(){
        $data['lista'] = $this->m_panel->get_table('users');
	    	$menu_header = $this->load->view('panel/menu_header', '', TRUE);
        $aside = $this->load->view('panel/left_menu', '', TRUE);
        $content = $this->load->view('panel/panel', $data, TRUE);
        $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                       'aside'=>$aside,
                                                       'content'=>$content,
                                                       'included_js'=>array('statics/js/libraries/form.js')));
	}

  public function save_user(){
    $post = $this->input->post('save');
    $this->m_panel->save_register('users', $post);
    redirect('panel');
  }

  public function save_poligono(){
    $poligono = $this->input->post('poligono');
    $id_user = $this->input->post('idUser');
    $this->m_panel->delete('poligonoIdUser',$id_user,'poligonos');
    $coordenadas = explode(",",$poligono);
    $max = count($coordenadas);
    $max = count($coordenadas);
    $medio = ($max-1)/2;
    $aux = 1;
    $x = 1;
    for($i = 0; $i<$medio; $i++){
      if($x%2 == 0){
      $data['poligonoLatitud'] =$coordenadas[$aux] ;
      $data['poligonoLongitud'] =$coordenadas[$i] ;
      $data['poligonoIdUser'] = $id_user;
      $this->m_panel->save_register('poligonos', $data);
      $aux++;
     }else{
       $data['poligonoLatitud'] = $coordenadas[$i];
       $data['poligonoLongitud'] = $coordenadas[$aux];
       $data['poligonoIdUser'] = $id_user;
       $this->m_panel->save_register('poligonos', $data);
       $aux++;
      }
      $x++;
    }

  }

  public function get_record($id_user){
    $res = $this->m_panel-> get_result('registroIdUser',$id_user,'registros');
    $htmlt="";
    foreach($res as $row):
      $htmlt.='  <tr>';
      $htmlt.='    <td>'.$row->registroStatus.'</td>';
      $htmlt.='    <td>'.$row->registroFecha.'</td>';
      $htmlt.='    <td>'.$row->registroLatitud.','.$row->registroLongitud.'</td>';
      $htmlt.='  </tr>';
    endforeach;
    $html ='<table class="table">';
    $html.='<thead>';
    $html.='  <tr>';
    $html.='    <th>Status</th>';
    $html.='    <th>Date</th>';
    $html.='    <th>Coordinates</th>';
    $html.='  </tr>';
    $html.='</thead>';
    $html.='<tbody>';
    $html.=$htmlt;
    $html.='</tbody>';
    $html.='</table>';
    echo  $html;
  }




}
