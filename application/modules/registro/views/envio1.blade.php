<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-beta6
* @link https://tabler.io
* Copyright 2018-2022 The Tabler Authors
* Copyright 2018-2022 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title></title>
    <!-- CSS files -->
    <link href="<?php echo base_url();?>template/tabler-main/demo/dist/css/tabler.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url();?>template/tabler-main/demo/dist/css/tabler-flags.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url();?>template/tabler-main/demo/dist/css/tabler-payments.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url();?>template/tabler-main/demo/dist/css/tabler-vendors.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url();?>template/tabler-main/demo/dist/css/demo.min.css" rel="stylesheet"/>
  </head>
  <body  class=" border-top-wide border-primary d-flex flex-column">
    <div class="page page-center">
      <div class="container-tight py-4">
        <div class="empty">
          <div class="empty-img">
              <!--img src="./static/illustrations/undraw_quitting_time_dm8t.svg" height="128"  alt=""-->
              <img src="<?php echo base_url()?>logo/logo.png" height="50"/>
          </div>
          <p class="empty-title">¡SE ENVIO UN EMAIL A TU CORREO ELECTRONICO PRA CONFIRMAR LA CUENTA!</p>
          <p class="empty-subtitle text-muted">
          Revisar en correos no deseados si no lo ve en su bandeja de entrada.
          </p>
          <div class="empty-action">
            
          </div>
        </div>
      </div>
    </div>
    <!-- Libs JS -->
    <!-- Tabler Core -->
    <script src="<?php echo base_url();?>template/tabler-main/demo/dist/js/tabler.min.js"></script>
    <script src="<?php echo base_url();?>template/tabler-main/demo/dist/js/demo.min.js"></script>
  </body>
</html>