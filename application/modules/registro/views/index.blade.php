@layout('layout')



@section('contenido')


<style>
    .login_in{
        padding: 10px;
    }

    @font-face {
    font-family: roboto_light;
    src: url(<?php echo base_url().'statics/css/Roboto/';?>Roboto-Light.ttf);
    }

    @font-face {
    font-family: roboto_bold;
    src: url(<?php echo base_url().'statics/css/Roboto/';?>Roboto-Bold.ttf);
    }

    div{
    font-family: roboto_light;
    }

    .titulos{
    font-family: roboto_bold;
    }
</style>


<div class="container-fluid">
    <div class="row align-items-center">

    <div class="col-12 login_in" align="center">
        <img src="<?php echo base_url()?>logo/logo.png" height="50"/>
    </div>

    </div>
</div>


<div class="container-fluid">

    <div class="row align-items-center ">

    <div class="col-md-4 offset-md-4 col-xs-12 col-sm-8 offset-sm-2 ">

        <form class="card login_in" action="<?php echo base_url();?>index.php/registro/save_reg" method="post">
        
        
        <figure class="text-center">
        <blockquote class="blockquote">
            <p class="titulos">¡REGISTRATE GRATIS!</p>
        </blockquote>
        <figcaption class="blockquote-footer titulos">
            Pruebalo 5 días gratis
        </figcaption>
        </figure>

            <div class="form-group">
                <label for="exampleInputEmail1">Nombre</label>
                <input type="text" class="form-control input-sm" id="nombre" name="nombre" aria-describedby="" placeholder="" required>
                <small id="emailHelp" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Apellido paterno</label>
                <input type="text" class="form-control input-sm" id="apelido_paterno" name="apellido_paterno" aria-describedby="" placeholder="" required>
                <small id="emailHelp" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Apellido materno</label>
                <input type="text" class="form-control input-sm" id="apellido_materno" name="apellido_materno" aria-describedby="" placeholder="" required>
                <small id="emailHelp" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">País</label>
                <select class="form-select input-sm">
                    <option> México</option>
                    <option> USA</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Teléfono celular</label>
                <input type="number" class="form-control input-sm" id="exampleInputEmail1" aria-describedby="" placeholder="" >
                <small id="emailHelp" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control input-sm" name="email" id="exampleInputEmail1" aria-describedby="" placeholder="" >
                <small id="emailHelp" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control input-sm"  name="password" id="exampleInputPassword1" placeholder="" required>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Plan</label>
                <select class="form-select input-sm" id="id_plan" name="id_plan">
                    <option value="1"> PLAN BASICO</option>
                    <option value="2"> PLAN COMPLETO</option>
                    <option value="3" selected> PLAN IMITADO</option>
                </select>
            </div>

            <br/>
            
            <button type="submit" class="btn btn-success">Registrarme</button>
        </form>


    </div>

    

    </div>

</div>




<div class="container-fluid">
    <div class="row align-items-center">

    <div class="col-12 login_in" align="center">
        <img src="<?php echo base_url()?>logo/logo.png" height="50"/>
    </div>

    </div>
</div>


@endsection