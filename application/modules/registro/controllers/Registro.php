<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Registro extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        //$this->load->model('Minicio', '', TRUE);
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url','general'));

        date_default_timezone_set('America/Mexico_City');
        

    }

    


    public function reg(){
        $datos['index'] = '';
        $datos['cuentas'] = '';
        $datos['error'] = '';

        $this->blade->render('index_new',$datos);  
        
    }

    public function save_reg(){

        $datos['index'] = '';
        $datos['cuentas'] = '';
        $datos['error'] = '';

        if($this->input->post()){

            $this->form_validation->set_rules('nombre', 'nombre', 'required');
            $this->form_validation->set_rules('apellido_paterno', 'apellido paterno', 'required');
            $this->form_validation->set_rules('apellido_materno', 'apellido materno', 'required');
      
            $response = validate($this);
      
      
            if($response['status']){

              


              $cuenta['id_id'] = get_guid();
              $cuenta['fecha_creacion'] = date('Y-m-d H:s:i');
              $cuenta['status'] = 5;
              $cuenta['id_plan'] = $this->input->post('id_plan');
              $cuenta['token'] = $cuenta['id_id'].'-'.generate_token();
              $id_cuenta = $this->Mgeneral->save_register('cuenta', $cuenta);
              $cuenta_nueva = $this->Mgeneral->get_row('id',$id_cuenta,'cuenta');

              $usuario['id_id'] = get_guid();
              $usuario['nombre'] = $this->input->post('nombre');
              $usuario['apellido_paterno'] = $this->input->post('apellido_paterno');
              $usuario['apellido_materno'] = $this->input->post('apellido_materno');
              $usuario['telefono'] = $this->input->post('telefono');
              $usuario['password'] = $this->input->post('password');
              $usuario['email'] = $this->input->post('email');
              $usuario['status'] = 5;
              $usuario['token'] = $cuenta['token'];
              $usuario['fecha_creacion'] = date('Y-m-d H:s:i');
              $id_usuario = $this->Mgeneral->save_register('usuarios', $usuario);
              $usuario_nuevo = $this->Mgeneral->get_row('id',$id_usuario,'usuarios');

              $usuario_cuenta['id_usuario'] = $id_usuario;
              $usuario_cuenta['id_id_usuario'] = $usuario_nuevo->id_id;
              $usuario_cuenta['id_cuenta'] = $id_cuenta;
              $usuario_cuenta['id_id_cuenta'] = $cuenta_nueva->id_id;
              $usuario_cuenta['status'] = 5;
              $usuario_cuenta['id_tipo_cuenta'] = 1;
              $usuario_cuenta['token'] = $cuenta['token'];
              $usuario_cuenta['fecha_creacion'] = date('Y-m-d H:s:i');
              $this->Mgeneral->save_register('usuarios_cuenta', $usuario_cuenta);


              redirect('registro/email/'.$usuario_cuenta['token'].'/'.$usuario['id_id']);
              
      
              
              //$this->Mgeneral->update_table_row('actividades',$data,'actividadId',$id);
            }
          //  echo $response;
          //echo json_encode(array('output' => $response));
          

        }else{

            //$this->blade->render('index',$datos);
        }

    }
    public function activar_cuenta($token){


    }

    public function email($token,$id_id_usuario){
        $datos['index'] = '';
        $datos['cuentas'] = '';
        $datos['error'] = '';
        $usuario_nuevo = $this->Mgeneral->get_row('id_id',$id_id_usuario,'usuarios');
        $this->blade->render('envio1',$datos);  
        $this->enviar_correo($usuario_nuevo->id_id,$usuario_nuevo->email);
    }


    public function email_template(){
        
        $datos['nombre'] = 'Gregorio Jalomo';
        $datos['texto1'] = 'Por favor dar click en el boton para activar tu cuenta.';
        $datos['texto2'] = 'Si crees que este mensaje es un error puedes simplemente ignorarlo.';
        $datos['texto3'] = 'This is a really simple email template. Its sole purpose is to get the recipient to click the button with no distractions.';

        $this->blade->render('formato_email',$datos);  
        $this->enviar_correo(1,'liberiusg@gmail.com');

    }

    public function verificar($toke,$id_id){
        $usuario_datos = $this->Mgeneral->get_row('id_id',$id_id,'usuarios');
        if(is_object($usuario_datos)){

            if($usuario_datos->status == 5){
                $data['status'] = 1;
                $this->Mgeneral->update_table_row('usuarios',$data,'id_id',$id_id);
                $datos['texto1'] = "!TU CUENTA A SIDO VERIFICADA CORRECTAMENTE¡";
                $this->blade->render('verificar',$datos); 
            }else{
                $datos['texto1'] = "!CUENTA VERIFICADA ";
                $this->blade->render('verificar',$datos); 

            }

        }else{
            $datos['texto1'] = "!CUENTA VERIFICADA ";
            $this->blade->render('verificar',$datos); 

        }

    }

    public function enviar_correo($id_usuario,$email_usuario){

        $usuario_datos = $this->Mgeneral->get_row('id_id',$id_usuario,'usuarios');
        if(is_object($usuario_datos)){
            $datos['nombre'] = $usuario_datos->nombre.' '.$usuario_datos->apellido_paterno;
            $datos['url_verificar'] = base_url().'index.php/registro/verificar/'.$usuario_datos->token.'/'.$usuario_datos->id_id;
            $datos['texto1'] = 'Por favor dar click en el boton para activar tu cuenta.';
            $datos['texto2'] = 'Si crees que este mensaje es un error puedes simplemente ignorarlo.';
            $datos['texto3'] = 'This is a really simple email template. Its sole purpose is to get the recipient to click the button with no distractions.';


        }else{
            $datos['nombre'] = '';
            $datos['url_verificar'] = '';
            $datos['texto1'] = 'Por favor dar click en el boton para activar tu cuenta.';
            $datos['texto2'] = 'Si crees que este mensaje es un error puedes simplemente ignorarlo.';
            $datos['texto3'] = 'This is a really simple email template. Its sole purpose is to get the recipient to click the button with no distractions.';

        }

        $data['id_usuario'] = $id_usuario;

        
        //$this->blade->render('formato_email',$datos);
        $cuerpo = $this->blade->render('formato_email',$datos,true);//$this->load->view('inicio/email', $data, true);
    
        $mensaje_contacto =  $cuerpo;
    
            //$this->load->config('email');
            $this->load->library('email');
    
            /*$config = array();
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'mail.mexicoalimentario.events';
            $config['smtp_user'] = 'contacto@mexicoalimentario.events';
            $config['smtp_pass'] = 'iPIHYLL}Pn+d';
            $config['smtp_port'] = 465;
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            */
    
            $mail_config['smtp_host'] = 'localhost';
            $mail_config['smtp_port'] = '465';
            $mail_config['smtp_user'] = 'proyecto2022@globodi.com';
            //$mail_config['_smtp_auth'] = TRUE;
            $mail_config['smtp_pass'] = 'W&s!.jKt9zKa';
            //$mail_config['smtp_crypto'] = 'tls';
            $mail_config['protocol'] = 'sendmail';
            $mail_config['mailtype'] = 'html';
            $mail_config['send_multipart'] = FALSE;
            $mail_config['charset'] = 'utf-8';
            $mail_config['wordwrap'] = TRUE;
            $this->email->initialize($mail_config);
    
            $this->email->set_newline("\r\n");
    
            
            
            $from = 'proyecto2022@globodi.com';//$this->config->item('smtp_user');
            $to = $email_usuario;//'corganizador@mexicoalimentario.events';//$this->input->post('to');
            $subject = 'ACTIVAR CUENTA, PROYECTO2022';//$this->input->post('subject');
            $message = $mensaje_contacto;//$this->input->post('message');
    
            $this->email->set_newline("\r\n");
            $this->email->from($from);
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($message);
    
            if ($this->email->send()) {
                //echo 'Your Email has successfully been sent.';
            } else {
                show_error($this->email->print_debugger());
            }
    
      }
    
    


    
}