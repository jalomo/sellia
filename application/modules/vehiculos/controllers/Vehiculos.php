<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Vehiculos extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        //$this->load->model('Minicio', '', TRUE);
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url','general'));

        date_default_timezone_set('America/Mexico_City');
        if($this->session->userdata('logged_in')){}else{redirect('login_user/');}
        

    }

   


    public function lista(){
        $datos['sucursales'] = $this->Mgeneral->get_table('vehiculo');
        $datos['index'] = '';
        $datos['cuentas'] = '';
        $datos['error'] = '';

        $this->blade->render('vehiculos/lista',$datos);  
        
    }

    public function editar($id){
        $datos['row'] = $this->Mgeneral->get_row('id',$id,'vehiculo');
        $datos['index'] = '';
        $datos['cuentas'] = '';
        $datos['error'] = '';
        $datos['id_vehiculo'] = $id;

        $this->blade->render('vehiculos/editar',$datos);  
        
    }

    public function guarda(){
        $this->form_validation->set_rules('nombre', 'nombre', 'required');
        $this->form_validation->set_rules('marca', 'marca', 'required');
        $this->form_validation->set_rules('modelo', 'modelo', 'required');
        $this->form_validation->set_rules('placas', 'placas', 'required');
        $this->form_validation->set_rules('color', 'color', 'required');
       

        $response = validate($this);
  
  
        if($response['status']){
          $data['nombre'] = $this->input->post('nombre');
          $data['marca'] = $this->input->post('marca');
          $data['modelo'] = $this->input->post('modelo');
          $data['placas'] = $this->input->post('placas');
          $data['color'] = $this->input->post('color');
          $data['fecha_creacion'] = date('Y-m-d H:i:s');
         
  
          $this->Mgeneral->save_register('vehiculo', $data);
        }
      //  echo $response;
      echo json_encode(array('output' => $response));
    }

    public function guarda_editar($id){
        $this->form_validation->set_rules('nombre', 'nombre', 'required');
        $this->form_validation->set_rules('marca', 'marca', 'required');
        $this->form_validation->set_rules('modelo', 'modelo', 'required');
        $this->form_validation->set_rules('placas', 'placas', 'required');
        $this->form_validation->set_rules('color', 'color', 'required');
       

        $response = validate($this);
  
  
        if($response['status']){
          $data['nombre'] = $this->input->post('nombre');
          $data['marca'] = $this->input->post('marca');
          $data['modelo'] = $this->input->post('modelo');
          $data['placas'] = $this->input->post('placas');
          $data['color'] = $this->input->post('color');
          $data['fecha_creacion'] = date('Y-m-d H:i:s');
         
  
          $this->Mgeneral->update_table_row('vehiculo',$data,'id',$id);
        }
      //  echo $response;
      echo json_encode(array('output' => $response));
    }
}