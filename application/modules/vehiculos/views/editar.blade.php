@layout('layout_main')
@section('contenido')

<div class="container-xl">
    <div class="row row-cards">
        <div class="col-12">
            <div class="card">
            <form id="formulario1">

            <div class="modal-body">

              <div class="mb-3">
                <label class="form-label">Nombre</label>
                <input type="text" class="form-control"  placeholder="" name="nombre" value="<?php echo $row->nombre;?>">
              </div>            
            </div>

            <div class="modal-body">
              <div class="mb-3">
                <label class="form-label">Marca</label>
                <input type="text" class="form-control"  placeholder="" name="marca" value="<?php echo $row->marca;?>">
              </div>            
            </div>

            <div class="modal-body">
              <div class="mb-3">
                <label class="form-label">Modelo</label>
                <input type="text" class="form-control"  placeholder="" name="modelo" value="<?php echo $row->modelo;?>">
              </div>            
            </div>

            <div class="modal-body">
              <div class="mb-3">
                <label class="form-label">Color</label>
                <input type="text" class="form-control"  placeholder="" name="color" value="<?php echo $row->color;?>">
              </div>            
            </div>

            <div class="modal-body">
              <div class="mb-3">
                <label class="form-label">Placas</label>
                <input type="text" class="form-control"  placeholder="" name="placas" value="<?php echo $row->placas;?>">
              </div>            
            </div>

           


            
            <div class="modal-footer">
              
              <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                Cancelar
              </a>
              <a href="#" class="btn btn-primary ms-auto" data-bs-dismiss="modal" id="alta_crear1">
                <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
                Crear vehículo
              </a>
            </div>

          </form>
            </div>
        </div>
    </div>
</div>

@endsection



@section('header1')

<div class="container-xl">
          <!-- Page title -->
          <div class="page-header d-print-none">
            <div class="row align-items-center">
              <div class="col">
                <!-- Page pre-title -->
                <div class="page-pretitle">
                 
                </div>
               

                

              </div>
              <!-- Page title actions -->
              <div class="col-auto ms-auto d-print-none">
                <div class="btn-list">
                  <!--pan class="d-none d-sm-inline">
                    <a href="#" class="btn btn-white">
                      New view
                    </a>
                  </span-->
                  
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

@section('modales')



    <!-- equipo -->



    

@endsection


@section('javascript')
<script src="<?php echo base_url();?>statics/js/libraries/jquery.js"></script>
<script src="<?php echo base_url();?>statics/js/bootbox.min.js"></script>
<script src="<?php echo base_url();?>statics/js/general.js"></script>
<script>


$('#alta_crear1').click(function(event){
  event.preventDefault();
  $("#btn_guardar").hide();
  $("#btn_cargando").show();
  var url_sis ="<?php echo base_url()?>index.php/vehiculos/guarda_editar/<?php echo $id_vehiculo?>";

  // Get form
        var form = $('#formulario1')[0];

		// Create an FormData object
        var data = new FormData(form);

  $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: url_sis,
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (result) {
                console.log(result);
                json_response = JSON.parse(result);
                obj_output = json_response.output;
                obj_status = obj_output.status;
                if(obj_status == false){
                  aux = "";
                  $.each( obj_output.errors, function( key, value ) {
                    aux +=value+"<br/>";
                  });
                  exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
                  $("#btn_guardar").show();
                  $("#btn_cargando").hide();
                }
                if(obj_status == true){
                  exito_redirect("GUARDADO CON EXITO","success","<?php echo base_url()?>index.php/vehiculos/lista");
                }

            },
            error: function (e) {
                $("#btn_guardar").show();
                $("#btn_cargando").hide();
                //$("#result").text(e.responseText);
                //console.log("ERROR : ", e);
                //$("#btnSubmit").prop("disabled", false);
                exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
                

            }
        });
});

</script>
@endsection


