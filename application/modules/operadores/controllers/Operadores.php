<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Operadores extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        //$this->load->model('Minicio', '', TRUE);
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->model('Madmin', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url','general'));

        date_default_timezone_set('America/Mexico_City');

        if($this->session->userdata('logged_in')){}else{redirect('login_user/');}
        

    }

    public function lista(){
        $datos['rows'] = $this->Mgeneral->get_table('operadores');
        $datos['sucursales'] = $this->Mgeneral->get_table('sucursales');
        $datos['vehiculos'] = $this->Mgeneral->get_table('vehiculo');
        
        $datos['index'] = '';
        $datos['cuentas'] = '';
        $datos['error'] = '';

        $this->blade->render('operadores/lista',$datos);  
        
    }

    public function guarda(){
        $this->form_validation->set_rules('nombre', 'nombre', 'required');
        $this->form_validation->set_rules('telefono', 'telefono', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
       
        $response = validate($this);
  
  
        if($response['status']){
          $data['nombre_completo'] = $this->input->post('nombre');
          $data['telefono'] = $this->input->post('telefono');
          $data['password'] = $this->input->post('password');
          $data['fecha_creacion'] = date('Y-m-d H:i:s');
          $data['id_sucursal'] = $this->input->post('id_sucursal');
          $data['id_vehiculo'] = $this->input->post('id_vehiculo');
         
  
          $this->Mgeneral->save_register('operadores', $data);
        }
      //  echo $response;
      echo json_encode(array('output' => $response));
    }

    public function editar($id){
      $datos['row'] = $this->Mgeneral->get_row('id',$id,'operadores');
      $datos['sucursales'] = $this->Mgeneral->get_table('sucursales');
      $datos['vehiculos'] = $this->Mgeneral->get_table('vehiculo');
      
      $datos['index'] = '';
      $datos['cuentas'] = '';
      $datos['error'] = '';
      $datos['id_operador'] = $id;

      $this->blade->render('operadores/editar',$datos);  
      
  }

  public function guarda_editar($id){
    $this->form_validation->set_rules('nombre', 'nombre', 'required');
    $this->form_validation->set_rules('telefono', 'telefono', 'required');
    $this->form_validation->set_rules('password', 'password', 'required');
   
    $response = validate($this);


    if($response['status']){
      $data['nombre_completo'] = $this->input->post('nombre');
      $data['telefono'] = $this->input->post('telefono');
      $data['password'] = $this->input->post('password');
      $data['fecha_creacion'] = date('Y-m-d H:i:s');
      $data['id_sucursal'] = $this->input->post('id_sucursal');
      $data['id_vehiculo'] = $this->input->post('id_vehiculo');
     

      $this->Mgeneral->update_table_row('operadores',$data,'id',$id);
    }
  //  echo $response;
  echo json_encode(array('output' => $response));
}

}