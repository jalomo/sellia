<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Api extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        //$this->load->model('Minicio', '', TRUE);
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->model('Madmin', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url','general','companies'));

        date_default_timezone_set('America/Mexico_City');
        

    }

    public function crear_sucursal(){
       
       $llave = $this->config->item('key_api');
       $token =  $this->getBearerToken(); 

       $response = array();

       if($token != $llave){
        header("HTTP/1.1 401 Llave invalida");
       }else{

            $json = file_get_contents('php://input');
            if(isset($json)){

                $campos_json = json_decode($json);
                if( (!isset($campos_json->nombre)) || (!isset($campos_json->domicilio)) || (!isset($campos_json->latitud)) || (!isset($campos_json->longitud)) || (!isset($campos_json->estatus)) ){
                    header("HTTP/1.1 400 Error json");
                    $response['error'] = 1;
                    $response['mensaje'] = "campos incompletos";
                    echo json_encode($response);
                }else{
                    

                    if( ($campos_json->estatus == 1) || ($campos_json->estatus == 0)){
                        header("HTTP/1.1 200 Exito");
                        $data['nombre'] = $campos_json->nombre;
                        $data['domicilio'] = $campos_json->domicilio;
                        $data['latitud'] = $campos_json->latitud;
                        $data['longitud'] = $campos_json->longitud;
                        $data['status'] = $campos_json->estatus;
                        $data['fecha_creacion'] = date('Y-m-d H:i:s');

                        $id_sucursal = $this->Mgeneral->save_register('sucursales', $data);

                        $response['error'] = 0;
                        $response['mensaje'] = "sucursal creada con exito";
                        $response['id_sucursal'] = $id_sucursal;
                        echo json_encode($response);
                        
                    }else{
                        header("HTTP/1.1 400 Error json");
                        $response['error'] = 1;
                        $response['mensaje'] = "estatus debe ser 1 o 0";
                        echo json_encode($response);
                    }
                    
                }

            }else{
                header("HTTP/1.1 400 Error");

            }
        

       }
       
        
    }
    

    /*
    Actualizar sucursal
    estatus: 0: produccion, 1.-sanbox
    */
    public function actualizar_sucursal(){
       
        $llave = $this->config->item('key_api');
        $token =  $this->getBearerToken(); 
 
        $response = array();
 
        if($token != $llave){
         header("HTTP/1.1 401 Llave invalida");
        }else{
 
             $json = file_get_contents('php://input');
             if(isset($json)){
 
                 $campos_json = json_decode($json);
                 if( (!isset($campos_json->nombre)) || (!isset($campos_json->domicilio)) || (!isset($campos_json->latitud)) || (!isset($campos_json->longitud)) || (!isset($campos_json->estatus)) || (!isset($campos_json->id)) ){
                     header("HTTP/1.1 400 Error json");
                     $response['error'] = 1;
                     $response['mensaje'] = "campos incompletos";
                     echo json_encode($response);
                 }else{
                     
                     if( ($campos_json->estatus == 1) || ($campos_json->estatus == 0)){
                        $valida_sucursal = $this->Mgeneral->get_row('id',$campos_json->id,'sucursales');
                        if(is_object($valida_sucursal)){

                           header("HTTP/1.1 200 Exito");
                           $data['nombre'] = $campos_json->nombre;
                           $data['domicilio'] = $campos_json->domicilio;
                           $data['latitud'] = $campos_json->latitud;
                           $data['longitud'] = $campos_json->longitud;
                           $data['status'] = $campos_json->estatus;
                           $data['fecha_creacion'] = date('Y-m-d H:i:s');
   
                           $this->Mgeneral->update_table_row('sucursales',$data,'id',$campos_json->id);
   
                           $response['error'] = 0;
                           $response['mensaje'] = "sucursal actualizada con exito";
                           $response['id_sucursal'] = $campos_json->id;
                           echo json_encode($response);

                        }else{
                           header("HTTP/1.1 400 Error json");
                           $response['error'] = 1;
                           $response['mensaje'] = "el id de la sucursal no existe";
                           echo json_encode($response);

                        }
 
                     }else{

                        header("HTTP/1.1 400 Error json");
                        $response['error'] = 1;
                        $response['mensaje'] = "esatus debe ser 1 o 0";
                        echo json_encode($response);
 
                     }
                     
 
                     
                     
 
                 }
 
             }else{
                 header("HTTP/1.1 400 Error");
 
             }
         
 
        }
        
         
     }

    /*
        tipo_pedido = 1= MOTO, 2=CAMIONETA
         estatus_pedido = 1= COTIZACION, 2=PEDIDO REAL
    */ 
    public function crear_pedido(){
       
        $llave = $this->config->item('key_api');
        $token =  $this->getBearerToken(); 
 
        $response = array();
 
        if($token != $llave){
         header("HTTP/1.1 401 Llave invalida");
        }else{
 
             $json = file_get_contents('php://input');
             if(isset($json)){
 
                 $campos_json = json_decode($json);
                 if( (!isset($campos_json->id_sucursal)) 
                     || (!isset($campos_json->folio_pedido)) 
                     || (!isset($campos_json->descripcion)) 
                     || (!isset($campos_json->nombre_cliente))
                     || (!isset($campos_json->latitud))
                     || (!isset($campos_json->longitud))
                     //|| (!isset($campos_json->tipo_pedido))
                     || (!isset($campos_json->estatus_pedido)) ){

                     header("HTTP/1.1 400 Error json");
                     $response['error'] = 1;
                     $response['mensaje'] = "campos incompletos";
                     echo json_encode($response);
                 }else{
                     
                     $sucursal = $this->Mgeneral->get_row('id',$campos_json->id_sucursal,'sucursales');
                     if(!is_object($sucursal)){
                        header("HTTP/1.1 400 Sucursal no existe");
                        $response['error'] = 1;
                        $response['mensaje'] = "Sucursal no existe dentro del sistema";
                        echo json_encode($response);
                        die();
                     }

                     $data['id_sucursal'] = $campos_json->id_sucursal;
                     $data['folio_pedido'] = $campos_json->folio_pedido;
                     $data['descripcion'] = $campos_json->descripcion;
                     $data['nombre_cliente'] = $campos_json->nombre_cliente;
                     $data['latitud'] = $campos_json->latitud;
                     $data['longitud'] = $campos_json->longitud;
                     $data['tipo_pedido'] = 0;
                     $data['estatus_pedido'] = $campos_json->estatus_pedido;
                     $data['direccion'] = $campos_json->direccion;
                     $data['m_longitud'] = $campos_json->m_longitud;
                     $data['m_anchura'] = $campos_json->anchura;
                     $data['m_altura'] = $campos_json->altura;
                     $data['m_peso_aproximado'] = $campos_json->peso_aproximado;

                     $precio = rand(50, 400);

                     $data['precio'] = $precio;
                     $data['estatus'] = 0;
                     $data['id_operador'] = 0;
                     $data['fecha_creacion'] = date('Y-m-d H:i:s');
                     if($campos_json->estatus_pedido == 1){

                        header("HTTP/1.1 200 Exito");
                        $response['error'] = 0;
                        $response['mensaje'] = "cotizacion";
                        $response['id_pedido'] = "";
   
                        $response['nombre_operador'] = "";
                        $response['costo_servicio'] = $precio;
                        echo json_encode($response);

                     }

                     if($campos_json->estatus_pedido == 2){
                        
                        header("HTTP/1.1 200 Exito");

                        $id_pedido = $this->Mgeneral->save_register('servicios', $data);
                         
                        $response['error'] = 0;
                        $response['mensaje'] = "Pedido creado";
                        $response['id_pedido'] = $id_pedido;

                        $response['nombre_operador'] = "JUAN PEREZ ORTEGA";
                        $response['costo_servicio'] = $precio;
                        echo json_encode($response);

                     }

                     if( ($campos_json->estatus_pedido != 1 ) && ($campos_json->estatus_pedido != 2 ) ){
                        header("HTTP/1.1 400 Error json");
                        $response['error'] = 1;
                        $response['mensaje'] = "estatus_pedido debe ser 1 o 2 (1=cotizacion,2=pedido real)";
                        echo json_encode($response);

                     }
                    
 
                 }
 
             }else{
                 header("HTTP/1.1 400 Error");
 
             }
         
 
        }
        
         
     }


    /** 
     * Get header Authorization
     * */
    function getAuthorizationHeader(){
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        }
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }

    /**
     * get access token from header
     * */
    function getBearerToken() {
        $headers = $this->getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }
}







