@layout('layout_main')



@section('contenido')
<div class="container-xl">
    <div class="row row-cards">
        <div class="col-12">
            <div class="card">
                <div class="table-responsive">


                <table class="table table-vcenter card-table">
                      <thead>
                        <tr>
                          <th>Nombre</th>
                          <th>Tipo</th>
                          <th>Raza</th>
                          <th>Categoria</th>
                          <th>Pastura</th>
                          <th class="w-1"></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td >Paweł Kuna</td>
                          <td class="text-muted" >
                            UI Designer, Training
                          </td>
                          <td class="text-muted" ><a href="#" class="text-reset">paweluna@howstuffworks.com</a></td>
                          <td class="text-muted" >
                            User
                          </td>
                          <td class="text-muted" >
                            User
                          </td>
                          <td>
                            <a href="#">Edit</a>
                          </td>
                        </tr>
                        
                      
                       
                        
                      </tbody>
                    </table>




                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('modales')


<div class="modal modal-blur fade" id="modal-animal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Nuevo animal</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div class="row mb-3">

              
              <div class="col-lg-6">
                <label class="form-label">Nombre</label>
                <input type="text" class="form-control " name="example-text-input" placeholder="">
              </div>
              <div class="col-lg-6">
                <label class="form-label">Número registro</label>
                <input type="text" class="form-control " name="example-text-input" placeholder="">
              </div>

            </div>

            <div class="row mb-3">

              <div class="col-lg-3">
                <label class="form-label">Etiqueta</label>
                <input type="text" class="form-control" name="example-text-input" placeholder="">
              </div>
              <div class="col-lg-3">
              <label class="form-label">Ubicación</label>
               <select class="form-select">
                   <option value="1">Oreja izquierda</option>
                   <option value="2">Oreja derecha</option>
                   <option value="3">Ambas orejas</option>
               </select>
              </div>

              <div class="col-lg-3">
                <label class="form-label">Fierro</label>
                <input type="text" class="form-control" name="example-text-input" placeholder="">
              </div>
              <div class="col-lg-3">
              <label class="form-label">Ubicación</label>
               <select class="form-select">
                   <option value="1">Oreja izquierda</option>
                   <option value="2">Oreja derecha</option>
                   <option value="3">Ambas orejas</option>
               </select>
              </div>

            </div>

            <div class="row mb-3">

              
              <div class="col-lg-6">
                <div class="form-label">Imagen fierro</div>
                <input type="file" class="form-control" />
              </div>
              <div class="col-lg-6">
                <img src=""/>
              </div>

            </div>

            <div class="row mb-3">

              
              <div class="col-lg-3">
                <label class="form-label">Estado</label>
                    <select class="form-select">
                    <option value="1">Activo</option>
                    <option value="2">Vendido</option>
                    <option value="3">Muerto</option>
                    <option value="3">Referencia</option>
                </select>
                </div>
              <div class="col-lg-3">
                <label class="form-label">Tipo</label>
                <select class="form-select">
                    <option value="1">Vata</option>
                    <option value="2">Toro</option>
                    <option value="3">Ternero</option>
                </select>
              </div>

              <div class="col-lg-3">
                <label class="form-label">Sexo</label>
                <select class="form-select">
                    <option value="1">Toro</option>
                    <option value="2">Novilla</option>
                    <option value="3">Steer</option>
                </select>
              </div>

              <div class="col-lg-3">
                <label class="form-label">Categoria</label>
                <select class="form-select">
                    <option value="1">Agregar una categoria</option>
                    
                </select>
              </div>

            </div>

            <hr/>

            <label class="form-label">Becerro</label>


            <div class="row mb-3">

              <div class="col-lg-3">
                    <label class="form-label">Fecha de nacimiento</label>
                    <input type="text" class="form-control" />
                </div>
              <div class="col-lg-3">
                <label class="form-label">Peso al nacer</label>
                <input type="text" class="form-control" />
              </div>

              <div class="col-lg-3">
                <label class="form-label">Fecha destete</label>
                <input type="text" class="form-control" />
              </div>

              <div class="col-lg-3">
                <label class="form-label">Peso al destete</label>
                <input type="text" class="form-control" />
              </div>

            </div>

            <div class="row mb-3">
                <div class="col-lg-3">
                    <label class="form-label">Fecha de un año</label>
                    <input type="text" class="form-control" />
                </div>
                <div class="col-lg-3">
                    <label class="form-label">Peso de un año</label>
                    <input type="text" class="form-control" />
                </div>
            </div>


            <hr/>
            <label class="form-label">Descripción del animal</label>


            <div class="row mb-3">
                <div class="col-lg-3">
                    <label class="form-label">Raza</label>
                    <select class="form-select">
                     <option value="1">Agregar una categoria</option>
                    
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="form-label">Peso de un año</label>
                    <select class="form-select">
                     <option value="1">Color</option>
                    
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="form-label">Concepción</label>
                    <select class="form-select">
                     <option value="1">Natural</option>
                     <option value="1">Inseminación artificial</option>
                     <option value="1">Transferfencia de embrión</option>
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="form-label">Becerro injertado</label>
                    <select class="form-select">
                     <option value="1">SI</option>
                     <option value="1">NO</option>
                    </select>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-lg-3">
                    <label class="form-label">Patre</label>
                    <select class="form-select">
                     <option value="1">Agregar una categoria</option>
                    
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="form-label">Madre</label>
                    <select class="form-select">
                     <option value="1">Color</option>
                    
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="form-label">Parentesco de ADN</label>
                    <select class="form-select">
                     <option value="1">Madre verificada</option>
                     <option value="1">Padre verificado</option>
                     <option value="1">Sin excluciones</option>
                     <option value="1">Ambos expluidos</option>
                     <option value="1">Madre excluida</option>
                     <option value="1">Padre excluido </option>
                     <option value="1">Muestra fallida</option>
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="form-label">Comentarios</label>
                    <input type="text" class="form-control" />
                </div>
            </div>


            <hr/>
            <label class="form-label">Información de la compra</label>


            <div class="row mb-3">
                <div class="col-lg-3">
                    <label class="form-label">Comprado</label>
                    <select class="form-select">
                     <option value="1">SI</option>
                     <option value="1">NO</option>
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="form-label">Propietario de la compra</label>
                    <select class="form-select">
                     <option value="1"></option>
                    
                    </select>
                </div>
                
                <div class="col-lg-3">
                    <label class="form-label">Comentarios</label>
                    <input type="text" class="form-control" />
                </div>
            </div>



              

            


            

            


            
          </div>
        
          <div class="modal-footer">
            <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
              Cancel
            </a>
            <a href="#" class="btn btn-primary ms-auto" data-bs-dismiss="modal">
              <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
              <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
              Crear nuevo animal
            </a>
          </div>
        </div>
      </div>
    </div>















@endsection