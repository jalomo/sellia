@layout('layout_main')
@section('contenido')

<div class="container-xl">
    <div class="row row-cards">
        <div class="col-12">
            <div class="card">
                <div class="table-responsive">

                <?php foreach($poligonos as $row):?>
         { lat: <?php echo $row->poligonoLatitud;?>, lng: <?php echo $row->poligonoLongitud;?> },
    <?php endforeach;?>

                    <div id="map" style="width:100%; height:400px !important;"></div>




                </div>
            </div>
        </div>
    </div>
</div>

@endsection



@section('header1')



@endsection

@section('modales')





    

@endsection


@section('javascript')
<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsGi7VFLa4OiA2ME9CkUj7zzvPUZVEeNU&callback=initMap&v=weekly"
      async
    ></script>

    <!--script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=drawing&key=AIzaSyDsGi7VFLa4OiA2ME9CkUj7zzvPUZVEeNU"></script-->

    <script src="<?php echo base_url();?>statics/js/libraries/jquery.js"></script>
<script>

// This example creates a simple polygon representing the Bermuda Triangle.
function initMap() {
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 13,
    center: { lat: 19.252857, lng: -103.726942 },
    mapTypeId: "terrain",
  });
  // Define the LatLng coordinates for the polygon's path.
  const triangleCoords = [
    <?php foreach($poligonos as $row):?>
         { lat: <?php echo $row->poligonoLatitud;?>, lng: <?php echo $row->poligonoLongitud;?> },
    <?php endforeach;?>
   
  ];
  // Construct the polygon.
  const bermudaTriangle = new google.maps.Polygon({
    paths: triangleCoords,
    strokeColor: "#FF0000",
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: "#FF0000",
    fillOpacity: 0.35,
  });

  bermudaTriangle.setMap(map);
}
</script>

@endsection

