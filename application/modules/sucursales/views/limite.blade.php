@layout('layout_main')
@section('contenido')

<div class="container-xl">
    <div class="row row-cards">
        <div class="col-12">
            <div class="card">
                <div class="table-responsive">

                    <div id="map-canvas" style="width:100%; height:400px !important;"></div>




                </div>
            </div>
        </div>
    </div>
</div>

@endsection



@section('header1')

<div class="container-xl">
          <!-- Page title -->
          <div class="page-header d-print-none">
            <div class="row align-items-center">
              <div class="col">
                <!-- Page pre-title -->
                <div class="page-pretitle">
                  Limite de sucursal
                </div>
               

                

              </div>
              <!-- Page title actions -->
              <div class="col-auto ms-auto d-print-none">
                <div class="btn-list">
                 
                  <a href="#" class="btn btn-primary " id="save_guardar">
                    <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
                   Guardar poligono sucursal
                  </a>
                 
                </div>
              </div>
            </div>
          </div>
</div>

@endsection

@section('modales')





    

@endsection


@section('javascript')
<script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=drawing&callback=initialize&key=AIzaSyDsGi7VFLa4OiA2ME9CkUj7zzvPUZVEeNU" async defer></script>

<!--script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=drawing&key=AIzaSyDsGi7VFLa4OiA2ME9CkUj7zzvPUZVEeNU"></script-->
<script src="<?php echo base_url();?>statics/js/libraries/jquery.js"></script>
<!--script>
var polygons = [];
var map; // Global declaration of the map
            var iw = new google.maps.InfoWindow(); // Global declaration of the infowindow
            var lat_longs = new Array();
            var markers = new Array();
            var drawingManager;
            function initialize() {
                 var myLatlng = new google.maps.LatLng(19.252857, -103.726942);
                var myOptions = {
                    zoom: 13,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP}
                map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
                drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.POLYGON,
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: [google.maps.drawing.OverlayType.POLYGON]
                },
                        polygonOptions: {
                            editable: true
                        }
            });
            drawingManager.setMap(map);

            google.maps.event.addListener(drawingManager, "overlaycomplete", function(event) {
                var newShape = event.overlay;
                newShape.type = event.type;
                 polygons.push(newShape);
            });

            google.maps.event.addListener(drawingManager, "overlaycomplete", function(event){
                overlayClickListener(event.overlay);
                $('#vertices').val(event.overlay.getPath().getArray());
            });
        }
function overlayClickListener(overlay) {
    google.maps.event.addListener(overlay, "mouseup", function(event){
        $('#vertices').val(overlay.getPath().getArray());
    });
}

google.maps.event.addDomListener(window, 'load', initialize);





$(function(){
    $('#save_guardar').click(function(){
        vertices = "";
        for (var i=0; i<polygons.length; i++) {
            //vertices += "polygon "+i+"<br>";
            for (var j=0; j<polygons[i].getPath().getLength(); j++) {
                vertices += polygons[i].getPath().getAt(j).toUrlValue(6)+",";
            }
        }

        value_json = $.ajax({
         type: "POST",
         url: '<?php echo base_url();?>index.php/sucursales/save_poligono',
         data:{poligono:vertices,idUser:<?php echo $id_sucursal?>},
         async: true,
         dataType: "text",
          success: function(data){
            alert('poligono agregado');
            location.reload();
          }
         }).responseText;

    });
});

function getuser(id){
  $("#id_user").val(id);
}

function get_recort(id){
  value_json = $.ajax({
   type: "GET",
   url: '<?php echo base_url();?>index.php/panel/get_record/'+id,
   async: true,
   dataType: "html",
    success: function(data){
      $("#load").html(data);
    }
   }).responseText;
}
</script-->

<script>
var polygons1 = [];

  function initialize() {
            var map = new google.maps.Map(document.getElementById('map-canvas'), {
                zoom: 10,
                center: new google.maps.LatLng(19.252857, -103.726942),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true,
                zoomControl: true
            });
            var polyOptions = {
                fillColor: '#0099FF',
                fillOpacity: 0.7,
                strokeColor: '#AA2143',
                strokeWeight: 2,
                editable: true
            };
            // Creates a drawing manager attached to the map that allows the user to draw Polygons
            drawingManager = new google.maps.drawing.DrawingManager({
                 drawingMode:google.maps.drawing.OverlayType.POLYGON,
                drawingControlOptions: {
                    drawingModes: [
                        google.maps.drawing.OverlayType.POLYGON
                    ]
                },
                polygonOptions: polyOptions,
                map: map
            });
            google.maps.Polygon.prototype.Contains = function(point) {
                var crossings = 0,
                    path = this.getPath();
                    
                // for each edge
                for (var i = 0; i < path.getLength(); i++) {
                    var a = path.getAt(i),
                        j = i + 1;
                    if (j >= path.getLength()) {
                        j = 0;
                    }
                    var b = path.getAt(j);
                    if (rayCrossesSegment(point, a, b)) {
                        crossings++;
                    }
                }

                // odd number of crossings?
                return (crossings % 2 == 1);

                function rayCrossesSegment(point, a, b) { 
                    var px = point.lng(),
                        py = point.lat(),
                        ax = a.lng(),
                        ay = a.lat(),
                        bx = b.lng(),
                        by = b.lat();
                    if (ay > by) {
                        ax = b.lng();
                        ay = b.lat();
                        bx = a.lng();
                        by = a.lat();
                    }
                    // alter longitude to cater for 180 degree crossings
                    if (px < 0) {
                        px += 360;
                    }
                    if (ax < 0) {
                        ax += 360;
                    }
                    if (bx < 0) {
                        bx += 360;
                    }

                    if (py == ay || py == by) py += 0.00000001;
                    if ((py > by || py < ay) || (px > Math.max(ax, bx))) return false;
                    if (px < Math.min(ax, bx)) return true;

                    var red = (ax != bx) ? ((by - ay) / (bx - ax)) : Infinity;
                    var blue = (ax != px) ? ((py - ay) / (px - ax)) : Infinity;
                    return (blue >= red);

                }

            };

            /*google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {

                if (polygon.Contains(marker.getPosition())) {
                  alert(polygon.);
                    alert('YES');
                } else {
                    alert('NO');
                }
            });*/
            //var polygons1 = [];
            
            google.maps.event.addListener(drawingManager, "overlaycomplete", function(event) {
                var newShape = event.overlay;
                newShape.type = event.type;
                polygons1.push(newShape);

                //alert(polygons1.length);
            });
            
            var marker = new google.maps.Marker({
                position: {
                    lat: 33.619003,
                    lng: -83.867405
                },
                map: map
            });
        }


$(function(){
    $('#save_guardar').click(function(){
        vertices = "";
        for (var i=0; i<polygons1.length; i++) {
            //vertices += "polygon "+i+"<br>";
            for (var j=0; j<polygons1[i].getPath().getLength(); j++) {
                vertices += polygons1[i].getPath().getAt(j).toUrlValue(6)+",";
            }
        }

        console.info(polygons1);

        value_json = $.ajax({
         type: "POST",
         url: '<?php echo base_url();?>index.php/sucursales/save_poligono',
         data:{poligono:vertices,idUser:<?php echo $id_sucursal?>},
         async: true,
         dataType: "text",
          success: function(data){
            alert('poligono agregado');
            //location.reload();
          }
         }).responseText;

    });
});

</script>

@endsection


