@layout('layout_main')
@section('contenido')

<div class="container-xl">
    <div class="row row-cards">
        <div class="col-12">
            <div class="card">
                <div class="table-responsive">

                    <div id="map-canvas" style="width:100%; height:400px !important;"></div>




                </div>
            </div>
        </div>
    </div>
</div>

@endsection



@section('header1')

<div class="container-xl">
          <!-- Page title -->
          <div class="page-header d-print-none">
            <div class="row align-items-center">
              <div class="col">
                <!-- Page pre-title -->
                <div class="page-pretitle">
                  Limite de zona
                </div>
               

                

              </div>
              <!-- Page title actions -->
              <div class="col-auto ms-auto d-print-none">
                <div class="btn-list">
                 
                  <a href="#" class="btn btn-primary " id="save_guardar">
                    <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
                   Guardar poligono zona
                  </a>
                 
                </div>
              </div>
            </div>
          </div>
</div>

@endsection

@section('modales')





    

@endsection


@section('javascript')
<!--script src="https://maps.googleapis.com/maps/api/js?v=3&libraries=drawing&callback=initialize&key=AIzaSyDsGi7VFLa4OiA2ME9CkUj7zzvPUZVEeNU" async defer></script-->

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=drawing&key=AIzaSyDsGi7VFLa4OiA2ME9CkUj7zzvPUZVEeNU"></script>
<script src="<?php echo base_url();?>statics/js/libraries/jquery.js"></script>
<script>
var map;

var polygons1 = [];

function initialize() {
  if (document.getElementById('map-canvas')) {

    var mapOptions = {
      zoom: 13,
      center: {
        lat: 19.252857,
        lng: -103.726942
      },
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var field = new google.maps.Polygon({
      paths: [],
      editable: true
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    var drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: google.maps.drawing.OverlayType.POLYGON,
      polygonOptions: {
        editable: false
      },
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        drawingModes: [google.maps.drawing.OverlayType.POLYGON]
      }
    });
    drawingManager.setMap(map);

    google.maps.event.addListener(drawingManager, "overlaycomplete", function(event) {
                var newShape = event.overlay;
                newShape.type = event.type;
                polygons1.push(newShape);

                alert(polygons1.length);
            });

    google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
      drawingManager.setOptions({
        drawingMode: null,
        drawingControlOptions: {
          position: google.maps.ControlPosition.TOP_CENTER,
          drawingModes: []
        }
      });
      field.setPath(polygon.getPath().getArray());
      polygon.setMap(null);
      polygon = null;
      field.setMap(map);

      google.maps.event.addListener(field.getPath(), 'set_at', function(index, obj) {
        // changed point, via map
        for (var i = 0; i < field.getPath().getLength(); i++) {
          console.log(field.getPath().getAt(i).toUrlValue(6));
        }
        console.log("a point has changed");
      });
      google.maps.event.addListener(field.getPath(), 'insert_at', function(index, obj) {
        // new point via map
        for (var i = 0; i < field.getPath().getLength(); i++) {
          console.log(field.getPath().getAt(i).toUrlValue(6));
        }
        console.log("a point has been added");
      });
      google.maps.event.addListener(field.getPath(), "remove_at", function(index, obj) {
        //removed point, via map
        for (var i = 0; i < field.getPath().getLength(); i++) {
          console.log(field.getPath().getAt(i).toUrlValue(6));
        }
        console.log("a point has been removed");
      });
    });
  }
}
google.maps.event.addDomListener(window, "load", initialize);




$(function(){
    $('#save_guardar').click(function(){
        vertices = "";
        for (var i=0; i<polygons1.length; i++) {
            //vertices += "polygon "+i+"<br>";
            for (var j=0; j<polygons1[i].getPath().getLength(); j++) {
                vertices += polygons1[i].getPath().getAt(j).toUrlValue(6)+",";
            }
        }

        value_json = $.ajax({
         type: "POST",
         url: '<?php echo base_url();?>index.php/sucursales/save_poligono1',
         data:{poligono:vertices,idUser:<?php echo $id_sucursal?>},
         async: true,
         dataType: "text",
          success: function(data){
            console.log(data);
            //alert('poligono agregado');
            //location.reload();
          }
         }).responseText;

    });
});

function getuser(id){
  $("#id_user").val(id);
}

function get_recort(id){
  value_json = $.ajax({
   type: "GET",
   url: '<?php echo base_url();?>index.php/panel/get_record/'+id,
   async: true,
   dataType: "html",
    success: function(data){
      $("#load").html(data);
    }
   }).responseText;
}
</script>



@endsection
