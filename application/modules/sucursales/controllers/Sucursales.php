<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Sucursales extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        //$this->load->model('Minicio', '', TRUE);
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url','general'));

        date_default_timezone_set('America/Mexico_City');
        

    }

    public function editar($id_sucursal){

      $sucursal = $this->Mgeneral->get_row('id',$id_sucursal,'sucursales');
      $datos['id_sucursal'] = $sucursal->id;
      $datos['index'] = '';
      $datos['cuentas'] = '';
      $datos['error'] = '';
      $datos['row'] = $sucursal;

      $this->blade->render('sucursales/editar',$datos);  

    }


    public function lista(){
        $datos['sucursales'] = $this->Mgeneral->get_table('sucursales');
        $datos['index'] = '';
        $datos['cuentas'] = '';
        $datos['error'] = '';

        $this->blade->render('sucursales/lista',$datos);  
        
    }

    public function limite($id_sucursal){
        $sucursal = $this->Mgeneral->get_row('id',$id_sucursal,'sucursales');
        $datos['id_sucursal'] = $sucursal->id;
        $datos['index'] = '';
        $datos['cuentas'] = '';
        $datos['error'] = '';

        $this->blade->render('sucursales/prueba',$datos);  
        
    }

    public function save_poligono(){
        $poligono = $this->input->post('poligono');
        var_dump($poligono);
        //die();
        $id_user = $this->input->post('idUser');
        $this->Mgeneral->delete('poligonoIdUser',$id_user,'poligonos_sucursales');
        $coordenadas = explode(",",$poligono);
        $max = count($coordenadas);
        $max = count($coordenadas);
        $medio = ($max-1)/2;
        $aux = 1;
        $aux2 = 0;
        $x = 1;
        for($i = 0; $i<$medio; $i++){
          if($x%2 == 0){
          $data['poligonoLatitud'] =$coordenadas[$aux2] ;
          $data['poligonoLongitud'] =$coordenadas[$aux] ;
          $data['poligonoIdUser'] = $id_user;
          $this->Mgeneral->save_register('poligonos_sucursales', $data);
          //$aux++;
         }else{
           $data['poligonoLatitud'] = $coordenadas[$aux2];
           $data['poligonoLongitud'] = $coordenadas[$aux];
           $data['poligonoIdUser'] = $id_user;
           $this->Mgeneral->save_register('poligonos_sucursales', $data);
          // $aux++;
          }
          $aux +=2;
          $aux2 +=2;
          $x++;
        }
    
      }

    public function prueba(){
        $datos['index'] = '';
        $datos['cuentas'] = '';
        $datos['error'] = '';

        $this->blade->render('sucursales/prueba',$datos);  
        
    }

    public function guarda(){
        $this->form_validation->set_rules('nombre', 'nombre', 'required');
        $this->form_validation->set_rules('domicilio', 'domicilio', 'required');
        $this->form_validation->set_rules('latitud', 'latitud', 'required');
        $this->form_validation->set_rules('longitud', 'longitud', 'required');
       
        $response = validate($this);
  
  
        if($response['status']){
          $data['nombre'] = $this->input->post('nombre');
          $data['domicilio'] = $this->input->post('domicilio');
          $data['latitud'] = $this->input->post('latitud');
          $data['longitud'] = $this->input->post('longitud');
         
  
          $this->Mgeneral->save_register('sucursales', $data);
        }
      //  echo $response;
      echo json_encode(array('output' => $response));
    }

    public function guarda_editar($id_sucursal){
      $this->form_validation->set_rules('nombre', 'nombre', 'required');
      $this->form_validation->set_rules('domicilio', 'domicilio', 'required');
      $this->form_validation->set_rules('latitud', 'latitud', 'required');
      $this->form_validation->set_rules('longitud', 'longitud', 'required');
     
      $response = validate($this);


      if($response['status']){
        $data['nombre'] = $this->input->post('nombre');
        $data['domicilio'] = $this->input->post('domicilio');
        $data['latitud'] = $this->input->post('latitud');
        $data['longitud'] = $this->input->post('longitud');
       

        $this->Mgeneral->update_table_row('sucursales',$data,'id',$id_sucursal);
      }
    //  echo $response;
    echo json_encode(array('output' => $response));
  }

    public function ver_zona($id_sucursal){

      $sucursal = $this->Mgeneral->get_row('id',$id_sucursal,'sucursales');
      $datos['poligonos'] = $this->Mgeneral->get_result('poligonoIdUser',$id_sucursal,'poligonos_sucursales');

      $datos['id_sucursal'] = $sucursal->id;
      $datos['index'] = '';
      $datos['cuentas'] = '';
      $datos['error'] = '';

      $this->blade->render('sucursales/ver_limites',$datos);  

    }

    public function limite_sucursal($id_sucursal){

      $sucursal = $this->Mgeneral->get_row('id',$id_sucursal,'sucursales');
      $datos['id_sucursal'] = $sucursal->id;
      $datos['index'] = '';
      $datos['cuentas'] = '';
      $datos['error'] = '';

      $this->blade->render('sucursales/limite_sucursal',$datos);  

    }

    public function prueba2(){
      $texto = "19.268738,-103.741877,19.266794,-103.703596,19.235191,-103.706686,19.230815,-103.748915";
      $coordenadas = explode(",",$texto);
      
        $max = count($coordenadas);
        $max = count($coordenadas);
        $medio = ($max-1)/2;
        $aux = 1;
        $aux2 = 0;
        $x = 1;
        for($i = 0; $i<$medio; $i++){
          echo $x%2;
         
          if($x%2 == 0){
          $data['poligonoLatitud'] =$coordenadas[$aux2] ;
          $data['poligonoLongitud'] =$coordenadas[$aux] ;


          
          
         }else{
           $data['poligonoLatitud'] = $coordenadas[$aux2];
           $data['poligonoLongitud'] = $coordenadas[$aux];
           var_dump($data);
           
           
           
          }

          $aux +=2;
           $aux2 +=2;
          
          //$aux++;
          $x++;

         
        }
    }


    public function agregar_zona($id_sucursal){
      $sucursal = $this->Mgeneral->get_row('id',$id_sucursal,'sucursales');
      $datos['id_sucursal'] = $sucursal->id;
      $datos['index'] = '';
      $datos['cuentas'] = '';
      $datos['error'] = '';

      $this->blade->render('sucursales/agregar_zona',$datos);  
      
  }


  public function save_poligono1(){
    $poligono = $this->input->post('poligono');
    var_dump($poligono);
    //die();
    $id_user = $this->input->post('idUser');
    $this->Mgeneral->delete('poligonoIdUser',$id_user,'poligonos_zonas');
    $coordenadas = explode(",",$poligono);
    $max = count($coordenadas);
    $max = count($coordenadas);
    $medio = ($max-1)/2;
    $aux = 1;
    $aux2 = 0;
    $x = 1;
    for($i = 0; $i<$medio; $i++){
      if($x%2 == 0){
      $data['poligonoLatitud'] =$coordenadas[$aux2] ;
      $data['poligonoLongitud'] =$coordenadas[$aux] ;
      $data['poligonoIdUser'] = $id_user;
      $this->Mgeneral->save_register('poligonos_zonas', $data);
      //$aux++;
     }else{
       $data['poligonoLatitud'] = $coordenadas[$aux2];
       $data['poligonoLongitud'] = $coordenadas[$aux];
       $data['poligonoIdUser'] = $id_user;
       $this->Mgeneral->save_register('poligonos_zonas', $data);
      // $aux++;
      }
      $aux +=2;
      $aux2 +=2;
      $x++;
    }

  }

  public function ver_zona1($id_sucursal){

    $sucursal = $this->Mgeneral->get_row('id',$id_sucursal,'sucursales');
    $datos['poligonos'] = $this->Mgeneral->get_result('poligonoIdUser',$id_sucursal,'poligonos_zonas');

    $datos['id_sucursal'] = $sucursal->id;
    $datos['index'] = '';
    $datos['cuentas'] = '';
    $datos['error'] = '';

    $this->blade->render('sucursales/ver_limites',$datos);  

  }

  public function prueba_mapa($status){

    $datos['id_sucursal'] = 1;
    $datos['index'] = '';
    $datos['cuentas'] = '';
    $datos['error'] = '';
    $datos['status'] = $status;

    $this->blade->render('sucursales/mapa',$datos);  

  }

  public function get_coordenadas($status){
   // header("Content-Security-Policy: upgrade-insecure-requests;");
    //header("Content-Security-Policy: default-src 'upgrade-insecure-requests'");

    $mapa_en_servicio = $this->Mgeneral->mapa_en_servicio($status);
    $array = array();
    foreach( $mapa_en_servicio as $user):
      /*$array[$user['id_operador']]['info']='<b>'.$user['fecha_creacion'].'</b>';

      $array[$user['id_operador']]['lat']= $user['latitud'];

      $array[$user['id_operador']]['lng']= $user['longitud'];
      */

      $array[$user->id]['info']='<b>'.$user->nick.'</b>';

      $array[$user->id]['lat']= $user->latitud;

      $array[$user->id]['lng']= $user->longitud;
    endforeach;
    echo json_encode($array);

   
  }

}