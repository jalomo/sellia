<?php
/**

 **/
class Madmin extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
    }

  

	public function save_register($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function actualizar_tabla($tabla,$id_tabla,$data, $id){
        $this->db->update($tabla, $data, array($id_tabla=>$id));
    }

    public function select_row($table,$id_table, $id){
      $this->db->where($id_table,$id);

      return $this->db->get($table)->row();

    }

    public function select_result_servicios($table,$id_table, $id){
      $this->db->where($id_table,$id);
      $this->db->order_by('id', 'DESC');
      return $this->db->get($table)->result();

    }


    public function select_result_servicios_dos($table,$id_table, $id){
      $this->db->where($id_table,$id);
      $this->db->order_by('id', 'DESC');
      return $this->db->get($table,10)->result();

    }

    public function select_result($table,$id_table, $id){
      $this->db->where($id_table,$id);
      return $this->db->get($table)->result();

    }

    public function get_tabla($tabla){

      return $this->db->get($tabla)->result();
    }
    // desc asc
    public function get_tabla_desc_asc($tabla,$campo_tabla,$tipo){
    return   $this->db->order_by($campo_tabla,$tipo)->limit(1000)->get($tabla)->result();
      //return $this->db->get($tabla)->result();
    }

    public function delete($campo,$value,$tabla){
  		return $this->db->where($campo,$value)->delete($tabla);
  	}

    public function get_nombre($id_categoria){
      $this->db->where('id',$id_categoria);
      $query = $this->db->get('categorias');
      if($query->num_rows()>0){
        $res = $query->row();
        return $res->nombre_categoria;
      }else{
        return "";
      }

    }


    public function get_nombre_comercio($id_comercio){
      $this->db->where('id',$id_comercio);
      $query = $this->db->get('comercios');
      if($query->num_rows()>0){
        $res = $query->row();
        return $res->nombre;
      }else{
        return "";
      }

    }

    public function nombre_operador($id_comercio){
      $this->db->where('id',$id_comercio);
      $query = $this->db->get('operador');
      if($query->num_rows()>0){
        $res = $query->row();
        return $res->nombre;
      }else{
        return "";
      }

    }

    public function get_id_row($table,$id_table,$id){
      $this->db->where($id_table,$id);
  		$query=$this->db->get($table);
  		return $query->row();

  	}

    public function actulizar_tabla($tabla,$data, $id_tabla, $id){
        $this->db->update($tabla, $data, array($id_tabla=>$id));
    }



    public function get_name_operador($id_operador){
      $this->db->where('id',$id_operador);
     $query = $this->db->get('operador');
     if($query->num_rows()>0){
      return $query->row()->nombre;
    }else{
      return "sin nombre";
    }



    }

    public function set_auto_operador($id_operador,$data){
      $this->db->where('id_operador',$id_operador);
      $this->db->where('status',1);
      $query = $this->db->get('auto_operador');
      if($query->num_rows()>0){
        $res = $query->row();
        $data_o['status'] = 2;
        $this->actulizar_tabla('auto_operador',$data_o, 'id', $res->id);
      }
      $this->save_register('auto_operador', $data);

    }

    public function get_placas($id_operador){
      $this->db->select('auto.placas');
      $this->db->from('auto');
      $this->db->join('auto_operador', 'auto_operador.id_auto = auto.id');
      $this->db->where('auto_operador.id_operador',$id_operador);
      //$this->db->where('auto_operador.status',1);
      $query = $this->db->get()->row();
      if(is_object($query)){
        return $query->placas;
      }else{
        return "";
      }

    }

    public function eliminar_operador($id){

          $this->db->delete('auto_operador', array('id_operador'=>$id));
          $this->db->delete('operador', array('id'=>$id));

    }

    public function get_operadores(){
      $this->db->select('auto_operador.latitud, auto_operador.longitud, operador.nombre, operador.usuario, operador.foto, operador.telefono, auto.placas,auto.nick, auto.foto, auto.color, auto.marca, auto.modelo,auto_operador.status');
      $this->db->from('auto_operador');
      $this->db->join('operador', 'operador.id = auto_operador.id_operador');
      $this->db->join('auto', 'auto.id = auto_operador.id_auto');
      return $this->db->get()->result();
    }

    /*
    * 1.- activos
      0.-inactivo
    */
    public function get_operadores_status($status,$nick){
      $this->db->select('auto_operador.id,auto_operador.latitud, auto_operador.longitud, operador.nombre, operador.usuario, operador.foto, operador.telefono, auto.placas,auto.nick, auto.foto as imagen, auto.color, auto.marca, auto.modelo,auto_operador.status');
      $this->db->from('auto_operador');
      $this->db->join('operador', 'operador.id = auto_operador.id_operador');
      $this->db->join('auto', 'auto.id = auto_operador.id_auto');
      if($status!=8){
        $this->db->where('auto_operador.status',$status);
      }
      if($nick!=null || $nick!="" ){
        $this->db->where('auto.nick',$nick);
      }

      return $this->db->get()->result();
    }



}
