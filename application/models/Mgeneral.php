<?php
/**

 **/
class Mgeneral extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
    }

    public function mapa_en_servicio($status, $nick = null)
    {
        if($status == 5){
          $this->db->where('status', $status);
         // $this->db->where('adminPassword', $pass);
          $data = $this->db->get('auto_operador')->result();
        return $data;

        }else{
          //$this->db->where('status', $status);
       // $this->db->where('adminPassword', $pass);
        //$data = $this->db->get('auto_operador')->result();
        //return $data;

        $this->db->select('auto_operador.id,auto_operador.latitud, auto_operador.longitud, operador.nombre, operador.usuario, operador.foto, operador.telefono, auto.placas,auto.nick, auto.foto as imagen, auto.color, auto.marca, auto.modelo,auto_operador.status');
          $this->db->from('auto_operador');
          $this->db->join('operador', 'operador.id = auto_operador.id_operador');
          $this->db->join('auto', 'auto.id = auto_operador.id_auto');
          if($status!=8){
            $this->db->where('auto_operador.status',$status);
          }
          if($nick!=null || $nick!="" ){
            $this->db->where('auto.nick',$nick);
          }

          return $this->db->get()->result();
        
        }
        
    }

    function getEmployees($postData=null){

      $response = array();
 
      ## Read value
      $draw = $postData['draw'];
      $start = $postData['start'];
      $rowperpage = $postData['length']; // Rows display per page
      $columnIndex = $postData['order'][0]['column']; // Column index
      $columnName = $postData['columns'][$columnIndex]['data']; // Column name
      $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
      $searchValue = $postData['search']['value']; // Search value
 
      ## Search 
      $searchQuery = "";
      if($searchValue != ''){
         $searchQuery = " (id like '%".$searchValue."%' or id_usuario like '%".$searchValue."%' or fecha_creacion like'%".$searchValue."%' ) ";
      }
 
      ## Total number of records without filtering
      $this->db->select('count(*) as allcount');
      $records = $this->db->get('servicios')->result();
      $totalRecords = $records[0]->allcount;
 
      ## Total number of record with filtering
      $this->db->select('count(*) as allcount');
      if($searchQuery != '')
         $this->db->where($searchQuery);
      $records = $this->db->get('servicios')->result();
      $totalRecordwithFilter = $records[0]->allcount;
 
      ## Fetch records
      $this->db->select('*');
      if($searchQuery != '')
         $this->db->where($searchQuery);
      $this->db->order_by($columnName, $columnSortOrder);
      $this->db->limit($rowperpage, $start);
      $records = $this->db->get('servicios')->result();
 
      $data = array();
 
      foreach($records as $record ){
 
         $data[] = array( 
            "id"=>$record->id,
            "usuario"=>nombre_de_usuario($record->id_usuario),
            "telefono"=>telefono_de_usuario($record->id_usuario),
            "fecha_creacion"=>$record->fecha_creacion,
            "status"=> $this->status_servicio($record->status),
            "nombre_operador"=> $this->nombre_operador($record->id_chofer),
            "tarifa"=>$record->tarifa,
            "inicio"=>$record->latitud.','.$record->longitud,
            "fin"=>$record->latitud_usuario.','.$record->longitud_usuario,
         ); 
      }
 
      ## Response
      $response = array(
         "draw" => intval($draw),
         "iTotalRecords" => $totalRecords,
         "iTotalDisplayRecords" => $totalRecordwithFilter,
         "aaData" => $data
      );
 
      return $response; 
    }

   function nombre_operador($id_comercio){
      $this->db->where('id',$id_comercio);
      $query = $this->db->get('operador');
      if($query->num_rows()>0){
        $res = $query->row();
        return $res->nombre;
      }else{
        return "";
      }

    }

    function status_servicio($status){
      $texto = "";
      switch ($status) {
        case 0:
          $texto = "Servicio creado";
            break;
        case 1:
          $texto = "Servicio Asignado";
            break;
        case 2:
          $texto = "Cancelado por chofer";
            break;
        case 3:
          $texto = "Cancelado por usuario";
            break;
        case 4:
          $texto = "Finalizado";
            break;
        case 5:
          $texto = "Sin choferes disponibles";
            break;
        case 6:
          $texto = "aviso de llegada al usuario";
            break;

      }
      return $texto;
    }



    function login($username, $password)
    {
      $this -> db -> select('id, username, password');
      $this -> db -> from('administradores');
      $this -> db -> where('username', $username);
      $this -> db -> where('password', MD5($password));
      $this -> db -> limit(1);

      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
        return $query->result();
      }
      else
      {
        return false;
      }
    }

    

    

  

    public function save_register($table, $data)
      {
          $this->db->insert($table, $data);
          return $this->db->insert_id();
      }

      public function delete_row($tabla,$id_tabla,$id){
		   $this->db->delete($tabla, array($id_tabla=>$id));
    	}

      public function delete($campo,$value,$tabla){
    		return $this->db->where($campo,$value)->delete($tabla);
    	}

      public function get_table($table){
    		$data = $this->db->get($table)->result();
    		return $data;
    	}

      public function get_row($campo,$value,$tabla){
    		return $this->db->where($campo,$value)->get($tabla)->row();
    	}

      public function get_result($campo,$value,$tabla){
    		return $this->db->where($campo,$value)->get($tabla)->result();
    	}

      public function update_table_row($tabla,$data,$id_table,$id){
    		$this->db->update($tabla, $data, array($id_table=>$id));
    	}

      public function update_permiso_rol($data,$permiso,$id_rol){
        $this->db->update('permisos_roles', $data, array("permisoCategoria"=>$permiso,"permisoIdRol"=>$id_rol));

      }

      public function save_admin($data){
    		$this->db->insert('admin', $data);
            return $this->db->insert_id();
    	}

      public function count_results_users($user, $pass)
        {
            $this->db->where('adminUsername', $user);
            $this->db->where('adminPassword', $pass);
            $total = $this->db->count_all_results('admin');
            return $total;
        }

        public function get_user_login($user, $pass)
        {
            $this->db->where('adminUsername', $user);
            $this->db->where('adminPassword', $pass);
            $total = $this->db->get('admin')->row();
            return $total;
        }

        public function get_all_data_users_specific($username, $pass)
          {
              $this->db->where('adminUsername', $username);
              $this->db->where('adminPassword', $pass);
              $data = $this->db->get('admin');
              return $data->row();
          }

        public function login_usuario($user, $pass)
        {
            $this->db->where('email', $user);
            $this->db->where('password', $pass);
            $total = $this->db->get('usuarios')->row();
            return $total;
        }

      
}
