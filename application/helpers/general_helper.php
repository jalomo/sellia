<?php
function get_guid()
{
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        return $charid;
}

function generate_token(){
        $val = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                  mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
                  mt_rand( 0, 0xffff ),
                  mt_rand( 0, 0x0C2f ) | 0x4000,
                  mt_rand( 0, 0x3fff ) | 0x8000,
                  mt_rand( 0, 0x2Aff ), mt_rand( 0, 0xffD3 ), mt_rand( 0, 0xff4B )
              );
        return strtoupper($val);
}


function nombre_sucursal($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('sucursales')
           ->where('id', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    if(is_object($nombre)){
        return $nombre->nombre;
      
    }else{
      return "";
    }
    
}

function nombr_vehiculo($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('vehiculo')
           ->where('id', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    if(is_object($nombre)){
        return $nombre->nombre;
      
    }else{
      return 0;
    }
    
}


function nombr_operador($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('operadores')
           ->where('id', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    if(is_object($nombre)){
        return $nombre->nombre_completo;
      
    }else{
      return 0;
    }
    
}
